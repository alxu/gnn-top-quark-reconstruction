from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from setuptools import find_packages
from setuptools import setup

description="Use Graph Network to perform analysis"

setup(
    name="root_gnn",
    version="0.0.1",
    description="Library for using Graph Nural Networks in HEP analysis",
    long_description=description,
    author="Xiangyang Ju",
    license="Apache License, Version 2.0",
    keywords=["graph networks", "HEP", "analysis", "machine learning"],
    url="https://github.com/xju2/root_gnn",
    packages=find_packages(),
    install_requires=[
        "future",
        "numpy",
        "scipy",
        "pandas",
        "setuptools",
        "six",
        "matplotlib",
        "sklearn",
        "pyyaml>=5.1",
        "graph_nets==1.0.5",
        'tensorflow-estimator==1.15.1',
        'tensorflow-gpu==1.15.0',
        'tensorflow==1.15.0',
        'tensorflow-probability==0.8.0',
        "networkx==2.3",
    ],
    setup_requires=['graph_nets'],
    classifiers=[
        "Programming Language :: Python :: 3.7",
    ],
    scripts=[
        'scripts/train_classifier',
        'scripts/view_training_log',
    ],
)
