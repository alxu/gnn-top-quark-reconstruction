#!/bin/bash
#SBATCH -q shared
#SBATCH -t 4:00:00
#SBATCH --mem=30GB
#SBATCH -C haswell
#SBATCH -A m3443

rundir=$1
model=$2
file=$3
i=$4
is_ttH=$5

thresh=$6
score_criteria=$7
overlap=$8
num_triplets=$9

cd $rundir
source ~/software/miniconda2/bin/activate env

printf '\nTop reco: model %s\nthresh %.2f, score_criteria %s, overlap %s\n' $model $thresh $score_criteria $overlap  
printf '\nStarting: %s\n' $file
printf '\nSaving progress in: %s\n' $rundir

python -u top_candidates.py --csv $file --is_ttH $is_ttH --thresh $thresh --score_criteria $score_criteria --overlap $overlap --num_triplets $num_triplets> $i.txt





