# root_gnn
## GNN classification for reconstructed HEP events stored in ROOT

A software package that implements the Graph Neural Network for top quark reconstruction using TensorFlow.

## Workflow

#### Install
```
git clone https://gitlab.cern.ch/alxu/gnn-top-quark-reconstruction.git
cd gnn-top-quark-reconstruction/

module load python
conda create --name <env_name> python=3.7
conda activate <env_name>
pip install -e .
conda install -c conda-forge root
```

#### Generate graphs:
* Saves pickled graphs under trained_results/\<model name\>/id

1. configs/config.ini
    * Change "[Training] root_directory" to be the path of current working directory
    * Change other parameters as appropriate: See below description for more information
2. Run r2g_script.py
    * Runs locally by default
    * Uncomment line 55 to instead submit as batch job
```
python scripts/r2g_script.py
```

#### Run training:
* Saves TF checkpoints and log under trained_results/\<model name\>/td/
* Uses config file from graph generation

1. run_train_batch.sh: Setup bash script and virtual environment
2. Submit batch job or run locally
```
./run_train_batch_submit.sh
```

#### View training performance:
* Saves plot under plots_training/
```
view_training_log <path to training log> <output png file name>
```

#### Run application:
* Saves edge scores into csv under evaluate_csv/\<model name\>/\<sample name\>/

1. configs/config_eval.ini: Change "[Model] root_directory" to be the path of current working directory
2. run_eval_batch.sh: Setup bash script and virtual environment
3. Submit batch job or run locally
```
./run_eval_batch_submit.sh
```

#### Reconstruct top candidates:
* Saves highest scoring top candidates into csv under evaluate_csv/\<model name\>/\<sample name\>/

1. run_eval_batch.sh: Setup bash script and virtual environment
2. Submit batch job or run locally
```
./run_topreco_batch_submit.sh
```

#### Calculate efficiency:
1. run_topreco_eval.sh: Setup virtual environment activate
2. Run script
```
./run_topreco_eval.sh
```

## Config file description

#### Samples.ini
The samples config file keeps track of the different samples used for graph generation, training, and application. They are referred to using the section names. To add new samples, save them under a new subdirectory and add the paths under a new section. If these are ttH events, include "ttH" in the section name; otherwise, do not. Then, run the command below, which saves a "fold" variable that is used for the train-test split.
```
python scripts/ntuplew_gnn_rndm.py --sample_name <section name>
```

#### Config.ini
This config file is used for graph generation and training.By default, graph generation uses "[Training] sample" and "[Preselection]", but these can be specified separately as optional arguments to r2g_script.py.  
  
* Training
    * sample: Section name of training sample in samples.ini
    * model_name
    * root_directory
    * model_directory: Directory with graphs and checkpoints from training
    * input_directory: Directory with graphs
    * training_directory: Directory with checkpoints from training
    * file_size: How many graphs to save in one file
    * log_sec: How often to log model performance during training (in seconds)
    * fold: Specifies which fold to use for training and test sets
* Preselection for training
    * use_pre_set1: Use preselection of best-performing GNN
    * use_pre_other: Use pre_other for preselection
    * pre_other: User-defined preselection
        * e.g. 3Nj_Nbj_0Nl1_2Tm
        * 3 <= m_njet, 0 <= m_nlep < 1 (i.e. m_nlep == 0), 2 <= n_truth
* Graph construction
    * Solution: Whether to use node, edge, global targets
    * Nodes: Whether to include different node features
    * Edges: Whether to include different edge features
    * Global: Whether to include different global features
* Hyperparameters for training

#### Config_eval.ini
This config file is used for application.  

* Application sample
* Model
    * model_name
    * root_directory
    * output_directory
    * model_directory
    * checkpoint: Model checkpoint to use for application, default uses the last saved checkpoint
* Preselection for application
