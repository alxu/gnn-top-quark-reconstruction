#!/bin/bash

root_dir=$PWD
model=test
model_plot_label=test
sample=h025_ttH
preselection=3Nj_Nbj_0Nl1_Tm

thresh=0.02
score_criteria=sum
overlap=False
num_triplets=2

save_figs=False
calculate_efficiency=True

source ~/software/miniconda2/bin/activate env

python scripts/top_candidates_eval.py --model $model --model_plot $model_plot_label --sample $sample --preselection $preselection --thresh $thresh --score_criteria $score_criteria --overlap $overlap --num_triplets $num_triplets --save_figs $save_figs --calculate_efficiency $calculate_efficiency
