#!/usr/bin/env python
from __future__ import print_function
import ROOT
import pandas as pd
import numpy as np
import time, os
import re
import numpy.lib.recfunctions as rfn


def reco_top_candidates(csv_name, is_ttH, save_path, thresh, score_criteria, overlap, num_triplets):
    
    t0 = time.time()
    df = pd.read_csv(csv_name)    
    df = df.rename(columns = {'edge_pred':'pred', 'edge_target':'target'})

    cols_dtype = {c:'int64' for c in ['event_num', 'run_num']}
    cols_dtype = {**cols_dtype, **{c:'int8' for c in ['sender', 'receiver', 'target'] if c in df.columns}}
    df = df.astype(dtype = cols_dtype)

    # Drop duplicate edges within each event
    jets = ['sender', 'receiver']
    df.loc[:, 'jet_1'] = df[jets].min(axis = 1)
    df.loc[:, 'jet_2'] = df[jets].max(axis = 1)
    df2 = df.sort_values('pred', ascending = False).drop_duplicates(['event_num', 'run_num', 'jet_1', 'jet_2'])    
    df2 = df2.drop(columns = ['jet_1', 'jet_2'])
    
    df_pass = df2[df2['pred'] > thresh]
    del(df)
    del(df2)
    
    # Construct triplets
    df3 = pd.merge(df_pass, df_pass, on = ['event_num', 'run_num'], suffixes = ('_x', '_y'))    
    df3 = df3.query('(sender_x == sender_y) | (sender_x == receiver_y) | (receiver_x == sender_y) | (receiver_x == receiver_y)')
    df3 = df3.query('~((sender_x == sender_y) & (receiver_x == receiver_y))')
    
    def convert_to_jets(df):
        
        # Identify duplicated jet among the 2 pairs of jets
        jets = ['sender_x', 'receiver_x', 'sender_y', 'receiver_y']
        df.loc[:, 'jet_1'] = df[jets].min(axis = 1)
        df.loc[:, 'jet_3'] = df[jets].max(axis = 1)

        sxsy = np.array(df['sender_x'] == df['sender_y'])
        sxry = np.array(df['sender_x'] == df['receiver_y'])
        rxsy = np.array(df['receiver_x'] == df['sender_y'])
        rxry = np.array(df['receiver_x'] == df['receiver_y'])

        df.loc[:, 'jet_dupl'] = np.nan
        df.loc[sxsy | sxry, 'jet_dupl'] = df.loc[sxsy | sxry, 'sender_x']
        df.loc[rxsy | rxry, 'jet_dupl'] = df.loc[rxsy | rxry, 'receiver_x']
        df.loc[:, 'jet_2'] = df[jets].sum(axis = 1) - df['jet_1'] - df['jet_3'] - df['jet_dupl']
        
        df = df.astype(dtype = {'jet_dupl':'int8', 'jet_2':'int8'})

        # Relabel jets for merging
        jets = ['jet_1', 'jet_2', 'jet_3', 'jet_dupl']
        
        j2j3 = np.array(df['jet_1'] == df['jet_dupl'])
        j1j3 = np.array(df['jet_2'] == df['jet_dupl'])
        j1j2 = np.array(df['jet_3'] == df['jet_dupl'])

        df.loc[:, 'jet_nodup_1'] = np.nan
        df.loc[:, 'jet_nodup_2'] = np.nan
        df.loc[j2j3, 'jet_nodup_1'] = df.loc[j2j3, 'jet_2']
        df.loc[j2j3, 'jet_nodup_2'] = df.loc[j2j3, 'jet_3']
        df.loc[j1j3, 'jet_nodup_1'] = df.loc[j1j3, 'jet_1']
        df.loc[j1j3, 'jet_nodup_2'] = df.loc[j1j3, 'jet_3']
        df.loc[j1j2, 'jet_nodup_1'] = df.loc[j1j2, 'jet_1']
        df.loc[j1j2, 'jet_nodup_2'] = df.loc[j1j2, 'jet_2']

        df = df.astype(dtype = {c:'int8' for c in ['jet_nodup_1', 'jet_nodup_2']})
        df = df.drop(columns = ['jet_1', 'jet_2', 'jet_3'])
        df = df.rename(columns = {'jet_nodup_1': 'jet_1', 'jet_nodup_2':'jet_2'})

        return df
    
    
    df4 = convert_to_jets(df3)
    del(df3)
    
    # Relabel jets for merging
    jets = ['sender', 'receiver']
    df_pass.loc[:, 'jet_1'] = df_pass[jets].min(axis = 1)
    df_pass.loc[:, 'jet_2'] = df_pass[jets].max(axis = 1)
    
    df_final = pd.merge(df4, df_pass, on = ['event_num', 'run_num', 'jet_1', 'jet_2'])
    df_final = df_final.rename(columns = {'sender':'sender_z', 'receiver':'receiver_z', 'pred': 'pred_z', 'target': 'target_z'})
    
    # Relabel jets in increasing order
    jets = ['jet_1', 'jet_2', 'jet_dupl']
    df_final.loc[:, 'jet_min'] = df_final[jets].min(axis = 1)
    df_final.loc[:, 'jet_max'] = df_final[jets].max(axis = 1)
    df_final.loc[:, 'jet_mid'] = df_final[jets].sum(axis = 1) - df_final['jet_min'] - df_final['jet_max']
    
    df_final = df_final.drop(columns = jets)
    df_final = df_final.rename(columns = {'jet_min': 'jet_1', 'jet_mid': 'jet_2', 'jet_max': 'jet_3'})
    cols = df_final.columns
    cols_fix_order = cols[:-2].tolist() + [cols[-1], cols[-2]]
    df_final = df_final[cols_fix_order]
    
    
    def relabel_pred_target(df):
        jets = [['sender_' + x, 'receiver_' + x] for x in ['x', 'y', 'z']]

        # Sort senders and receivers
        for s, r in jets:
            pair = [s, r]
            df.loc[:, s + 'temp'] = df[pair].min(axis = 1)
            df.loc[:, r + 'temp'] = df[pair].max(axis = 1)
            df = df.drop(columns = pair)
            df = df.rename(columns = {s + 'temp': s, r + 'temp': r})

        x12 = np.array((df['sender_x'] == df['jet_1']) & (df['receiver_x'] == df['jet_2']))
        x23 = np.array((df['sender_x'] == df['jet_2']) & (df['receiver_x'] == df['jet_3']))
        x13 = np.array((df['sender_x'] == df['jet_1']) & (df['receiver_x'] == df['jet_3']))

        y12 = np.array((df['sender_y'] == df['jet_1']) & (df['receiver_y'] == df['jet_2']))
        y23 = np.array((df['sender_y'] == df['jet_2']) & (df['receiver_y'] == df['jet_3']))
        y13 = np.array((df['sender_y'] == df['jet_1']) & (df['receiver_y'] == df['jet_3']))

        z12 = np.array((df['sender_z'] == df['jet_1']) & (df['receiver_z'] == df['jet_2']))
        z23 = np.array((df['sender_z'] == df['jet_2']) & (df['receiver_z'] == df['jet_3']))
        z13 = np.array((df['sender_z'] == df['jet_1']) & (df['receiver_z'] == df['jet_3']))

        suffixes = ['12', '23', '13']
        sr = [[], [], []]
        sr[0] = [x12, y12, z12]
        sr[1] = [x23, y23, z23]
        sr[2] = [x13, y13, z13]

        for i in range(3):
            x, y, z = sr[i]
            suffix = suffixes[i]
            pred_new = 'pred_' + suffix
            target_new = 'target_' + suffix

            df.loc[:, pred_new] = -1
            df.loc[:, target_new] = -1

            df.loc[x, pred_new] = df.loc[x, 'pred_x']
            df.loc[y, pred_new] = df.loc[y, 'pred_y']
            df.loc[z, pred_new] = df.loc[z, 'pred_z']
            
            if is_ttH:
                df.loc[x, target_new] = df.loc[x, 'target_x']
                df.loc[y, target_new] = df.loc[y, 'target_y']
                df.loc[z, target_new] = df.loc[z, 'target_z']

        cols_to_drop = [c for c in cols if ('x' in c) or ('y' in c) or ('z' in c)]
        df = df.drop(columns = cols_to_drop)
        return df
    
    
    df_final = relabel_pred_target(df_final)
    
    if score_criteria == 'sum':
        pred_tripl = 'pred_sum'
        df_final[pred_tripl] = df_final['pred_12'] + df_final['pred_23'] + df_final['pred_13']
    else: # score_criteria == 'product'
        pred_tripl = 'pred_product'
        df_final[pred_tripl] = df_final['pred_12'] * df_final['pred_23'] * df_final['pred_13']
    
    # Drop duplicate triplets within each event
    df_final = df_final.sort_values(pred_tripl, ascending = False).drop_duplicates(subset = ['event_num', 'run_num', 'jet_1', 'jet_2', 'jet_3']) 
    

    # Select top num_triplet scoring triplets per event 
    if overlap:
        df_final = df_final.groupby(['event_num', 'run_num']).apply(lambda x: x.sort_values(pred_tripl, ascending = False).iloc[:num_triplets]) 
        df_final = df_final.reset_index(drop = True)
    else:
        s = (df_final['event_num'].value_counts() > 1)
        evts_one = [evt for evt in s.index if not s[evt]]
        evts_multiple = [evt for evt in s.index if s[evt]]
        
        df_final_one = df_final[df_final['event_num'].isin(evts_one)]
        df_final_multiple = df_final[df_final['event_num'].isin(evts_multiple)]
        
        topn = pd.DataFrame(columns = df_final.columns)
        num_evts = len(evts_multiple)
        
        i = 0 # # cumulative count of triplets
        for e in evts_multiple:
            j = 0 # triplet count in event
            jets_used = []
            evt = df_final[df_final['event_num'] == e]

            for k, row in evt.iterrows():
                no_overlap = (row['jet_1'] not in jets_used) and (row['jet_2'] not in jets_used) and (row['jet_3'] not in jets_used)
                if no_overlap:
                    topn.loc[i] = evt.loc[k]
                    jets_used += [row['jet_1'], row['jet_2'], row['jet_3']]
                    j += 1
                    i += 1
                    if j == num_triplets:
                        break
        
        topn = pd.concat([topn, df_final_one])
        
        cols = ['target_12', 'target_23', 'target_13', 'jet_1', 'jet_2', 'jet_3']
        cols_dtype = {c:'int64' for c in ['event_num', 'run_num']}
        cols_dtype = {**cols_dtype, **{c:'int8' for c in cols if c in topn.columns}}
        topn = topn.astype(dtype = cols_dtype)
    
    
    if save_path == '':
        csv_dir = re.split(r'\/\w+\.csv', csv_name)[0]
        csv_old_name = re.split(r'\/', csv_name)[-1]
        csv_new_name = re.findall(r'(\w+_)\w+.csv', csv_old_name)[0] + 'thresh' + str(int(thresh * 100)) + '_' + score_criteria
        
        if overlap:
            full_path = os.path.join(csv_dir, csv_new_name + '_topreco_top' + str(num_triplets) + '.csv')
        else:
            full_path = os.path.join(csv_dir, csv_new_name + '_topreco_exclusive_top' + str(num_triplets) + '.csv')
    else:
        full_path = save_path
    print('Saving:', full_path)
    
    if overlap:
        df_final.to_csv(full_path, index = False)
    else:
        topn.to_csv(full_path, index = False)
    print("Total time = {} seconds\n".format(time.time()-t0))
    

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Write GNN top candidates to csv')
    add_arg = parser.add_argument
    
    add_arg('--csv', type=str, help='csv with gnn scores')
    add_arg('--is_ttH', type=str, help='whether application sample is ttH')
    add_arg('--save_path', type=str, help='path to save csv', default='')
    add_arg('--thresh', type=float, help='edge score threshold', default=0.02)
    add_arg('--score_criteria', type=str, help='criteria to score triplets', default='sum')
    add_arg('--overlap', type=str, help='whether triplets within an event can have overlapping jets', default='False')
    add_arg('--num_triplets', type=int, help='how many triplets to reconstruct per event', default=2)
    
    args = parser.parse_args()
    
    valid_criteria = ['sum', 'product']
    if args.score_criteria not in valid_criteria:
        raise ValueError('Invalid score criteria, must be one of the following: {}'.format(valid_criteria))
    
    is_ttH = (args.is_ttH == 'True')
    overlap = (args.overlap == 'True')
    reco_top_candidates(args.csv, is_ttH, args.save_path, args.thresh, args.score_criteria, overlap, args.num_triplets)
 