# !/usr/bin/env python

import os
import glob, re
import itertools
import networkx as nx
import numpy as np
import math
import pandas as pd
from sklearn.utils import shuffle

from graph_nets import utils_tf
from graph_nets import utils_np

from root_gnn import dataset_top_reco as dataset
from root_gnn import utils_train
from root_gnn import prepare
import tensorflow as tf

import argparse
import configparser
import shutil

import time
start_time = time.time()

parser = argparse.ArgumentParser(description='evaluating a classifier')
add_arg = parser.add_argument
add_arg('--fold', type=int, help='fold')
add_arg('--config', type=str, help='config eval file')
add_arg('--samples', type=str, help='sample paths file')

args = parser.parse_args()
fold = args.fold

# Application config
config_file = args.config
config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config.read(config_file)

config_samples_file = args.samples
config_samples = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config_samples.read(config_samples_file)

application_sample = config['Application']['sample']
is_ttH = ('ttH' in application_sample)

output_dir = config['Model']['model_directory']
root_dir = config['Model']['root_directory']

# Save csv path
save_dir = os.path.join(root_dir, 'evaluate_csv', re.split(r'\/', output_dir)[-1], application_sample)
if os.path.exists(save_dir):
    config_file = os.path.join(save_dir, 'config_eval.ini')
else:
    os.makedirs(save_dir)
    config_file = shutil.copy(config_file, save_dir)
    
config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config.read(config_file)

# Training config
config_train_file = os.path.join(output_dir, 'config.ini')
config_train = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config_train.read(config_train_file)

use_global_solution = config_train.getboolean('Solution', 'global', fallback = False)
use_node_solution = config_train.getboolean('Solution', 'node', fallback = False)
use_edge_solution = config_train.getboolean('Solution', 'edge', fallback = False)

# Application
samples_root_dir = config_samples['General']['root_dir']
sample_sub_dir = config_samples[application_sample]['sub_dir']
tree_name = config_samples['General']['tree_name']

files_sample = list(dict(config_samples.items(application_sample)).keys())[1:]
file_names = [re.sub(r'file_', 'mc', f) for f in files_sample]
paths = list(dict(config_samples.items(application_sample)).values())[1:]
files_sample = [os.path.join(samples_root_dir, sample_sub_dir, p) for p in paths]

def prepare_graphs(data, is_training=True):
    all_graphs = []
    def get_graphs(input_data):
        nx_graph, evt_num, run_num = input_data.generate_nxgraph(is_training = is_training)
        input_g, target_g = prepare.graph_to_input_target(nx_graph, use_global_solution, use_node_solution, use_edge_solution)
        all_graphs.append((input_g, target_g, evt_num, run_num))

    get_graphs(data)

    input_graphs = [x[0] for x in all_graphs]
    target_graphs = [x[1] for x in all_graphs]
    evt_nums = [x[2] for x in all_graphs]
    run_nums = [x[3] for x in all_graphs]
    return input_graphs, target_graphs, evt_nums, run_nums

data_temp = dataset.dataset(files_sample[0], tree_name, config_train_file, config_file, ttH = is_ttH, fold = 0)

tf.reset_default_graph()
input_graphs, target_graphs, evt_nums, run_nums = prepare_graphs(data_temp, is_training=True)
input_ph  = utils_tf.placeholders_from_networkxs(input_graphs, force_dynamic_num_graphs=True)
target_ph = utils_tf.placeholders_from_networkxs(target_graphs, force_dynamic_num_graphs=True)


if use_global_solution and use_edge_solution:
    from root_gnn.model import EdgeGlobalClassifier as GeneralClassifier
elif use_node_solution and use_edge_solution:
    from root_gnn.model import NodeEdgeClassifier as GeneralClassifier
elif use_node_solution and not use_edge_solution:
    from root_gnn.model import NodeClassifier as GeneralClassifier
elif use_edge_solution:
    from root_gnn.model import GeneralClassifier


nl = config_train.getint('Hyperparameters', 'num_layers', fallback = 2)
ls = config_train.getint('Hyperparameters', 'latent_size', fallback = 128)
num_processing_steps_tr = config_train.getint('Hyperparameters', 'num_processing_steps', fallback = 4)

model = GeneralClassifier(num_layers = nl, latent_size = ls)
output_ops_tr = model(input_ph, num_processing_steps_tr)

output_dir = os.path.join(output_dir, 'td')
ckpt_name = 'checkpoint_{:05d}.ckpt'
files = glob.glob(output_dir+"/*.ckpt.meta")
last_iteration = 0 if len(files) < 1 else max([
                int(re.search('checkpoint_([0-9]*).ckpt.meta', os.path.basename(x)).group(1))
                for x in files])
print("last iteration:", last_iteration)

sess = tf.Session()
saver = tf.train.Saver()

checkpoint = config['Model']['checkpoint']
if eval(checkpoint) is None:
    print("loading checkpoint:", os.path.join(output_dir, ckpt_name.format(last_iteration)))
    saver.restore(sess, os.path.join(output_dir, ckpt_name.format(last_iteration)))
    
    config['Model']['checkpoint'] = str(last_iteration)
    with open(config_file, 'w') as cf:
        config.write(cf)
else:
    print("loading checkpoint:", os.path.join(output_dir, ckpt_name.format(int(checkpoint))))
    saver.restore(sess, os.path.join(output_dir, ckpt_name.format(int(checkpoint))))


def eval_edge_output(target, output):
    tdds = utils_np.graphs_tuple_to_data_dicts(target)
    odds = utils_np.graphs_tuple_to_data_dicts(output)
    
    test_target = []
    test_pred = []
    edges = []
    for td, od in zip(tdds, odds):
        test_target.append(td['edges'])
        test_pred.append(od['edges'])
        edges.append(list(zip(td['senders'], td['receivers'])))
    
    test_target = np.concatenate(test_target, axis=0)
    test_pred   = np.concatenate(test_pred,   axis=0)
    return test_pred, test_target, edges


def eval_edge_global_output(target, output):
    tdds = utils_np.graphs_tuple_to_data_dicts(target)
    odds = utils_np.graphs_tuple_to_data_dicts(output)
    
    n_edge = tdds[0]['n_edge']
    
    edge_target = []
    edge_pred = []
    edges = []
    global_target = []
    global_pred = []
    for td, od in zip(tdds, odds):
        edge_target.append(td['edges'])
        edge_pred.append(od['edges'])
        edges.append(list(zip(td['senders'],td['receivers'])))
        
        td_global = td['globals']
        od_global = od['globals']
        td_global = np.reshape([td_global[0]] * n_edge, (n_edge, 1))
        od_global = np.reshape([od_global[0]] * n_edge, (n_edge, 1))
        global_target.append(td_global)
        global_pred.append(od_global)
    
    edge_target   = np.concatenate(edge_target,   axis=0)
    edge_pred     = np.concatenate(edge_pred,     axis=0)
    global_target = np.concatenate(global_target, axis=0)
    global_pred   = np.concatenate(global_pred,   axis=0)
    return edge_pred, edge_target, edges, global_pred, global_target





def evaluate(d, n_batches, is_training, test_fold, csv_name):
    all_event_nums = []
    all_run_nums = []
    all_edge_pred = []
    all_edge_target = []
    all_edges = []
    all_global_pred = []
    all_global_target = []

    for i in range(n_batches):
        input_graphs, target_graphs, evt_nums, run_nums = prepare_graphs(d, is_training = is_training)
        input_graphs_ntuple = utils_np.networkxs_to_graphs_tuple(input_graphs)
        target_graphs_ntuple = utils_np.networkxs_to_graphs_tuple(target_graphs)
        
        if d.finished(is_training):
            print("\tFinished: {}, time {:.4f}".format(i, time.time() - start_time))
            break
        
        feed_dict = {
            input_ph: input_graphs_ntuple
        }
        test_values = sess.run({
            "outputs": output_ops_tr
        }, feed_dict=feed_dict)
        
        if use_edge_solution and use_global_solution:
            test_pred, test_target, edges, global_pred, global_target = eval_edge_global_output(target_graphs_ntuple, test_values['outputs'][-1])
        elif use_edge_solution:
            test_pred, test_target, edges = eval_edge_output(target_graphs_ntuple, test_values['outputs'][-1])
        else:
            raise RuntimeError('Application not supported.')
        
        all_event_nums.append(evt_nums)
        all_run_nums.append(run_nums)
        all_edge_pred.append(test_pred)
        all_edge_target.append(test_target)
        all_edges.append(edges)
        if use_global_solution:
            all_global_pred.append(global_pred)
            all_global_target.append(global_target)
            
        if (i % 25000 == 0) or (i + 1 == n_batches):
            print("\tFinished: {}, time {:.4f}".format(i, time.time() - start_time))
    
    
    print('Formatting output...time {:.4f}'.format(time.time() - start_time))
    
    test_pred = list(itertools.chain(*np.concatenate(all_edge_pred, axis=0)))
    test_target = list(itertools.chain(*np.concatenate(all_edge_target, axis=0)))
    test_event_nums = np.concatenate(all_event_nums, axis=0)
    test_run_nums = np.concatenate(all_run_nums, axis=0)
    if use_global_solution:
        test_global_pred = list(itertools.chain(*np.concatenate(all_global_pred, axis=0)))
        test_global_target = list(itertools.chain(*np.concatenate(all_global_target, axis=0)))
    
    test_sender = []
    test_receiver = []
    test_event_nums_dup = []
    test_run_nums_dup = []
    i = 0
    for e in all_edges:
        test_edges = np.concatenate(e, axis=0)
        test_sender += list(list(zip(*test_edges))[0])
        test_receiver += list(list(zip(*test_edges))[1])
        test_event_nums_dup += [test_event_nums[i]] * len(e[0])
        test_run_nums_dup += [test_run_nums[i]] * len(e[0])
        i += 1
    
    cols = ['event_num', 'run_num', 'sender', 'receiver', 'pred']
    cols_int = ['event_num', 'run_num', 'sender', 'receiver', 'target', 'global_target']
    data_to_save = [test_event_nums_dup, test_run_nums_dup, test_sender, test_receiver, test_pred]

    if is_ttH and use_global_solution:
        df = pd.DataFrame(data=data_to_save + [test_target, test_global_pred, test_global_target])
        cols += ['target', 'global_pred', 'global_target']
    elif is_ttH:
        df = pd.DataFrame(data=data_to_save + [test_target])
        cols += ['target']
    else:
        df = pd.DataFrame(data=data_to_save)
        
    df = df.T
    df.columns = cols
    cols_int = {c:'int64' for c in cols_int if c in cols}
    df = df.astype(dtype = cols_int)
    
    df = df.drop_duplicates()
    csv_name = '_'.join([csv_name, 'nodup.csv'])
        
    print('Saving data {}, fold {} at: {}'.format(name, test_fold, csv_name))
    df.to_csv(csv_name, index = False)
    

datasets = [dataset.dataset(f, tree_name, config_train_file, config_file, ttH = is_ttH, fold = fold) for f in files_sample]
data = list(zip(file_names, datasets))
test_fold = (fold + 4) % 5

use_pre_set1 = config.getboolean('Preselection', 'use_pre_set1')
use_pre_other = config.getboolean('Preselection', 'use_pre_other')

if use_pre_set1:
    pre_name = 'preSet1'
elif use_pre_other:
    pre_name = 'pre' + config['Preselection']['pre_other']
else:
    raise ValueError('Invalid application preselection.')

for name, d in data:
    csv = '_'.join([application_sample,
                    str(last_iteration if eval(checkpoint) is None else checkpoint), 
                    name,
                    'nps' + str(num_processing_steps_tr),
                    'fold' + str(test_fold),
                    pre_name])
    csv_name = os.path.join(save_dir, csv)
    
    print('\nStarting data {}, fold {}'.format(name, test_fold))
    n_batches_test = math.ceil(d.get_num_events() * 0.2)
    evaluate(d, n_batches_test, is_training = False, test_fold = test_fold, csv_name = csv_name)

sess.close()
print("--- %s seconds ---" % (time.time() - start_time))
