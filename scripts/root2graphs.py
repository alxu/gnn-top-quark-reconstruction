#!/usr/bin/env python

import os
import re
import argparse
import configparser
import pickle
import shutil

import argparse
import time
import numpy as np
from sklearn.utils import shuffle

import networkx as nx
from root_gnn import prepare
from root_gnn import dataset_top_reco as dataset

start_time = time.time()

parser = argparse.ArgumentParser(description='Saving converting .root to .graphs and .csv')
add_arg = parser.add_argument

add_arg('--file', type=str, help='file path')
add_arg('--name', type=str, help='file name')
add_arg('--sample', type=str, help='training sample')
add_arg('--preselection', type=str, help='training preselection')

add_arg('--is_training', type=int, help='True to get training events')
add_arg('--config_file', type=str, help='path to config file')

add_arg('--train_name', type=str, help='training files name', default = 'train')
add_arg('--test_name', type=str, help='test files name', default = 'test')


args = parser.parse_args()

file = args.file
name = args.name
sample = args.sample
preselection = args.preselection

is_training = args.is_training > 0
if is_training:
    is_training_name = args.train_name
else:
    is_training_name = args.test_name

config_file = args.config_file
config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config.read(config_file)
model_dir = config['Training']['model_directory']
outdir = config['Training']['input_directory']

file_size = config.getint('Training', 'file_size', fallback = 12800)
fold = config.getint('Training', 'fold', fallback = 0)
is_ttH = ('ttH' in sample)


if not os.path.exists(model_dir):
    os.makedirs(outdir)
    os.makedirs(config['Training']['training_directory'])
    shutil.copy(config_file, model_dir)
else:
    print('Using previously saved config file')
    config_file = os.path.join(model_dir, 'config.ini')
    config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
    config.read(config_file)

# Use config preselection
if preselection == 'None':
    preselection = ''
    
print('Saving graphs at: {}'.format(outdir))
data = dataset.dataset(file, 'output', config_file,
                       preselection = preselection, 
                       fold = fold, ttH = is_ttH)
    
use_global_solution = config.getboolean('Solution', 'global', fallback = False)
use_node_solution = config.getboolean('Solution', 'node', fallback = False)
use_edge_solution = config.getboolean('Solution', 'edge', fallback = False)

start_event = 0
data.idx_.tr_idx = start_event
data.idx_.te_idx = start_event

all_graphs = []
def get_graphs(input_data):
    for iph in range(file_size):
        nx_graph, eventNumber, runNumber = input_data.generate_nxgraph(is_training=is_training)
        if input_data.finished(is_training):
            break
        input_g, target_g = prepare.graph_to_input_target(nx_graph, use_global_solution, use_node_solution, use_edge_solution)
        all_graphs.append((input_g, target_g, eventNumber, runNumber))
    return None


while not data.finished(is_training):
    get_graphs(data)  
    all_graphs = shuffle(all_graphs)
    
    if is_training:
        end_event = data.idx_.tr_idx
    else:
        end_event = data.idx_.te_idx
    
    out_name = '_'.join([sample, name, is_training_name,
                         'fold' + str(fold), 'batch' + str(file_size), 
                         'start' + str(start_event), 'end' + str(end_event) + '.graphs'])
    outfile = open(os.path.join(outdir, out_name), 'wb+')
    pickle.dump(all_graphs, outfile)
    outfile.close()
    
    all_graphs = []
    end_time = time.time()
    print('Processed {} {} events. {:.4f} s elapsed.'.format(end_event, is_training_name, end_time - start_time))
    start_event = end_event + 1

print('Total time: {} s'.format(time.time() - start_time))