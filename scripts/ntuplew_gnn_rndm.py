#!/usr/bin/env python
from __future__ import print_function
import ROOT
import pandas as pd
import numpy as np
import time, os
import re
import configparser

from root_numpy import tree2array, array2root
import numpy.lib.recfunctions as rfn

def save_rndm(sample_name, config_samples, num_folds):
    
    config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
    config.read(config_samples)
    
    valid_samples = config.sections()[1:]
    if sample_name not in valid_samples:
        raise ValueError('Not a valid sample: {} \nMust be in: {}'.format(sample_name, valid_samples))
    
    root_dir = config['General']['root_dir']
    sample_sub_dir = config[sample_name]['sub_dir']
    tree_name = config['General']['tree_name']

    for file, path in config.items(sample_name):
        if file == 'sub_dir':
            continue

        out_path = re.sub(r'\.root', r'_rndm.root', path)
        config[sample_name][file] = out_path
        with open(config_samples, 'w') as cf:
            config.write(cf)
        
        in_file_name = os.path.join(root_dir, sample_sub_dir, path)
        out_file_name = os.path.join(root_dir, sample_sub_dir, out_path)

        if os.path.exists(out_file_name):
            os.system("rm " + out_file_name)
        os.system("cp " + in_file_name + " " + out_file_name)

        in_file = ROOT.TFile.Open(in_file_name, "READ")
        tree = in_file.Get(tree_name)
        n_events = tree.GetEntries()
        print("Sample {}, file {}, total events: {}".format(sample_name, file, n_events))

        all_events = tree2array(tree, branches = ['ph_eta1'])
        df = pd.DataFrame(all_events)
        df['fold'] = (df['ph_eta1'] * 1000000 % num_folds).astype(int)
        df['order'] = (df['ph_eta1'] * 1000000 % 1000).astype(int)

        names = ['fold', 'rndm_order']
        types = [('fold', 'i4'), ('rndm_order', 'i4')]
        output = np.full((n_events,), -1., dtype = types)

        output['fold'] = df['fold']
        output['rndm_order'] = df['order']

        array2root(output, out_file_name, tree_name, mode = "update")
        print('Saved', out_file_name)
    
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Write reproducible random numbers to ntuple')
    add_arg = parser.add_argument
    
    file_dir = os.path.dirname(os.path.realpath(__file__))
    default_dir = os.path.join(file_dir, '../configs/samples.ini')
    
    add_arg('--sample_name', type=str, help='sample to save numbers to')
    add_arg('--config_samples', type=str, help='sample paths file', default=default_dir)
    add_arg('--num_folds', type=int, help='number of folds', default=5)
    
    args = parser.parse_args()
    save_rndm(args.sample_name, args.config_samples, args.num_folds)
    