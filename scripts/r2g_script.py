#!/usr/bin/env python

import os
import re
import argparse
import configparser
import pickle
import time

start_time = time.time()
file_dir = os.path.dirname(os.path.realpath(__file__))
default_config_file = os.path.join(file_dir, '../configs/config.ini')
default_samples_file = os.path.join(file_dir, '../configs/samples.ini')

parser = argparse.ArgumentParser(description='Converting .root to .graphs')
add_arg = parser.add_argument

add_arg('--config_file', type=str, help='path to config file', default = default_config_file)
add_arg('--samples_file', type=str, help='path to samples file', default = default_samples_file)
add_arg('--sample', type=str, help='sample to override config sample', default = 'None')
add_arg('--preselection', type=str, help='preselection to override config preselection', default = 'None')

args = parser.parse_args()
config_file = args.config_file
samples_file = args.samples_file

config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
config.read(config_file)

outdir = config['Training']['input_directory']
model_dir = config['Training']['model_directory']
sample = config['Training']['sample']
preselection = args.preselection

if args.sample != 'None':
    print('Config training sample: {}, but will use input sample {}'.format(sample, args.sample))
    sample = args.sample

config_samples = configparser.ConfigParser()
config_samples.read(samples_file)    
tree_name = config_samples['General']['tree_name']
file_sub_dir = os.path.join(config_samples['General']['root_dir'],
                            config_samples[sample]['sub_dir'])
paths = list(dict(config_samples.items(sample)).values())[1:]
files_sample = [os.path.join(file_sub_dir, p) for p in paths]
file_names = [k[0] for k in config_samples.items(sample)][1:]
sample_names = [re.sub(r'file_', r'', n) for n in file_names]

r2g_path = os.path.join(file_dir, 'root2graphs.py')
r2g_batch = os.path.join(file_dir, '../run_root2graphs_batch.sh')

for f, name in zip(files_sample, sample_names):
    
    print('\nStarting next file: {}'.format(name))    
    # os.system('sbatch {} {} {} {} {} {} {}'.format(r2g_batch, r2g_path, f, name, sample, preselection, config_file))

    # Run locally
    os.system('python {} --file {} --name {} --sample {} --preselection {} --is_training 1 --config_file {}'.format(r2g_path, f, name, sample, preselection, config_file))
    os.system('python {} --file {} --name {} --sample {} --preselection {} --is_training 0 --config_file {}'.format(r2g_path, f, name, sample, preselection, config_file))


print('Total time: {} s'.format(time.time() - start_time))