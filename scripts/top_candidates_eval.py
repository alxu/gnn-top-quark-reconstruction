#!/usr/bin/env python
from __future__ import print_function
import ROOT
import pandas as pd
import numpy as np
from root_numpy import tree2array
import numpy.lib.recfunctions as rfn

import os
import re
import configparser
import glob
import time

from root_gnn import utils_topreco
from root_gnn import utils_root_eval
import sklearn.metrics

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def eval_top_candidates(model, model_plot, sample, preselection, thresh, score_criteria, overlap, num_triplets, save_figs, calculate_efficiency):
    
    t0 = time.time()
    
    file_dir = os.path.dirname(os.path.realpath(__file__))
    csv_dir = os.path.join(file_dir, '../evaluate_csv')
    samples_file = os.path.join(file_dir, '../configs/samples.ini')
    
    # Load configs
    samples_config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
    samples_config.read(samples_file)

    config_file = os.path.join(csv_dir, model, sample, 'config_eval.ini')
    if not os.path.isfile(config_file):
        raise ValueError('Cannot find config eval file under: {}.'.format(config_file))
    
    config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
    config.read(config_file)
    root_dir = config['Model']['root_directory']

    # Application sample
    samples_dir = samples_config['General']['root_dir']
    tree_name = samples_config['General']['tree_name']
    
    file_sub_dir = samples_config[sample]['sub_dir']
    sample = config['Application']['sample']
    is_ttH = ('ttH' in sample)
    
    file_names = [k[0] for k in samples_config.items(sample)][1:]
    sample_names = [re.sub(r'file_', r'', n) for n in file_names]
    branch_names = ['eventNumber', 'runNumber', 'fold', 
                    'm_njet', 'm_nbjet_fixed70', 'm_nlep',
                    'm_jet_pt', 'm_jet_eta', 'm_jet_phi', 'm_jet_E', 'm_jet_m']
    if is_ttH:
        branch_names += ['reco_triplet_1', 'reco_triplet_2']
    
    # Load samples and do preselection
    dfs_preselected = []
    for i in range(len(file_names)):
        f = samples_config[sample][file_names[i]]
        f = os.path.join(samples_dir, file_sub_dir, f)

        df = utils_root_eval.root_df(f, tree_name, branch_names, preselection, sample_names[i], is_ttH)
        dfs_preselected += [df]

    
    # Load saved application and top reconstruction
    criteria_dict = {'sum': 'pred_sum', 'product': 'pred_product'}
    score_criteria_col = criteria_dict[score_criteria]
    
    args = 'thresh{}_{}_topreco'.format(int(thresh * 100), score_criteria)
    if not overlap:
        args += '_exclusive'
    args += '_top' + str(num_triplets)

    csvs_topreco = glob.glob(os.path.join(csv_dir, model, sample) + '/*_mc16*_' + args + '.csv')
    csvs_topreco_ordered = []
    for s in sample_names:
        l = [c for c in csvs_topreco if s in c]
        l.sort()
        csvs_topreco_ordered.append(l)
    
    print('\nLoading top reco: {}'.format(args))
    dfs_topreco = []
    for i in range(len(csvs_topreco_ordered)):
        name = sample_names[i]
        s = csvs_topreco_ordered[i]
        s.sort()
        
        df_sample = pd.DataFrame()
        for j in range(len(s)):
            df = pd.read_csv(s[j])
            df.loc[:, 'fold'] = j
            df_sample = pd.concat([df_sample, df])
        
        print('Concatenating {} files in topreco sample {}: {}'.format(len(s), name, df_sample.shape))
        if len(s) != 5:
            raise OSError('Should be concatenating 5 files, but concatenated {} in sample {}'.format(len(s), name))
 
        dfs_topreco += [df_sample]
    
    
    print('\nLoading GNN scores')
    csvs_edge_ordered = [[re.findall(r'(.*)thresh\d+', c)[0] + 'nodup.csv' for c in sample] for sample in csvs_topreco_ordered]   
    dfs_edge = []
    for i in range(len(csvs_edge_ordered)):
        name = sample_names[i]
        s = csvs_edge_ordered[i]
        s.sort()

        df_sample = pd.DataFrame()
        for j in range(len(s)):
            df = pd.read_csv(s[j])
            df.loc[:, 'fold'] = j
            df_sample = pd.concat([df_sample, df])
        
        print('Concatenating {} files in edge sample {}: {}'.format(len(s), name, df_sample.shape))
        if len(s) != 5:
            raise OSError('Should be concatenating 5 files, but concatenated {} in sample {}'.format(len(s), name))
        dfs_edge += [df_sample]

    
    print('\nMerging top reco and root samples')
    i = 0
    fold = int(re.findall(r'fold(\d+)_', csvs_topreco_ordered[0][0])[0])
    test_fold = (fold + 4) % 5
    
    dfs_merge_train = []
    dfs_merge_test = []
    dfs_merge_efficiency = []
    for df_t, df_p in zip(dfs_topreco, dfs_preselected):
        df_m_tr, df_m_te, df_m_eff = utils_root_eval.merge_topreco_preselected(df_t, df_p, sample_names[i], is_ttH, test_fold, return_efficency_df = True)
        dfs_merge_train += [df_m_tr]
        dfs_merge_test += [df_m_te]
        dfs_merge_efficiency += [df_m_eff]
        i += 1
    
    if is_ttH and calculate_efficiency:
        utils_root_eval.calculate_efficiency_all(dfs_merge_efficiency, dfs_preselected, test_fold, sample)
    if not save_figs:
        return None
    
    df_merge = pd.DataFrame()
    for df_m in dfs_merge_train + dfs_merge_test:
        df_merge = pd.concat([df_merge, df_m])


    pre_edge = re.findall(r'(pre[A-Z][A-Za-z\d]+)', csvs_edge_ordered[0][0])
    if len(pre_edge) > 0:
        pre_edge = pre_edge[0]
    else:
        pre_edge = preselection
    
    # Setup plots
    buffer = 1e-6
    model_path = re.sub(r'\.', '', model)
    plots_dir = os.path.join(root_dir, 'plots_application', model_path, sample)
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
    print('\nSaving plots at: {}'.format(plots_dir))
            
    save_path_suffix = '_'.join(['model', model_plot, 'sample', sample, 'score' + str.title(score_criteria)])
    save_path_suffix_edge_all = '_'.join([save_path_suffix, pre_edge])
    save_path_suffix = '_'.join([save_path_suffix, preselection])
    save_path_suffix = '_'.join([save_path_suffix,
                                 'thresh' + str(int(thresh * 100)),
                                 'top' + str(num_triplets),
                                 'exclusive' + str(not overlap)[0],
                                 ])
        
    save_path_suffix_edge_all  = '_'.join([save_path_suffix_edge_all,
                                 'thresh' + str(int(thresh * 100)),
                                 'top' + str(num_triplets),
                                 'exclusive' + str(not overlap)[0],
                                 ])
    
    plot_title_thresh0 = 'model {}, sample {}\nthresh {}'.format(model_plot, sample, 0)
    plot_title = 'model {}, sample {} \nthresh {}, triplet edge {}'.format(model_plot, sample, thresh, score_criteria)
    if not overlap:
        plot_title += '\nmutually exclusive'
    

    
    # Plot distribution of events with different number of reconstructed triplets
    cols = ['eventNumber', 'runNumber', 'sample']
    df_merge_count = df_merge[~df_merge['event_num'].isna()]
    df_merge_count = df_merge_count[cols + [score_criteria_col]].groupby(cols).agg(len)
    df_merge_count = df_merge_count.reset_index().rename(columns = {score_criteria_col: 'n_reco'})
    count = df_merge_count['n_reco'].value_counts().sort_index()
    del(df_merge_count)
    
    count = count.to_dict()
    for i in range(1, 5):
        if i not in count.keys():
            count[i] = 0
    count0 = df_merge[df_merge['event_num'].isna()].shape[0]
    counts = np.append([count0], list(count.values()))
    
    save_path = os.path.join(plots_dir, '_'.join(['num_reco_tripl', save_path_suffix]))
    print('Saving distribution of number of reconstructed triplets')
    utils_topreco.plot_num_reco_tripl(counts, plot_title, save_path, save_figs)
    
    
    # Plot edge score distribution
    edge_preds = pd.DataFrame()
    for df_e in dfs_edge:
        edge_preds = pd.concat([edge_preds, df_e])

    if is_ttH:
        edge_preds = edge_preds[['pred', 'target']]
    else:
        edge_preds = edge_preds['pred']
    
    save_path = os.path.join(plots_dir, '_'.join(['edge_all', save_path_suffix_edge_all]))
    increment = 0.02
    print('Saving edge score distribution')
    utils_topreco.plot_edges_all(edge_preds, is_ttH, increment, buffer, plot_title_thresh0, save_path, save_figs)
    
    df_merge = df_merge[~df_merge['event_num'].isna()]
    cols_dtype = {c:'int64' for c in ['jet_1', 'jet_2', 'jet_3']}
    df_merge = df_merge.astype(dtype = cols_dtype)
    
    save_path = os.path.join(plots_dir, '_'.join(['edge', score_criteria, save_path_suffix]))
    legend_loc = 'upper left'
    print('Saving triplet score distribution')
    utils_topreco.plot_edge_scoring(df_merge, is_ttH, thresh, score_criteria, score_criteria_col, buffer, legend_loc, plot_title, save_path, save_figs)
    
    # Plot triplet mass distribution
    increment = 1e5
    print('\nComputing triplet mass...')
    triplet_m = utils_topreco.compute_triplet_mass(df_merge, increment)
    df_merge.loc[:, 'triplet_m'] = triplet_m
    save_path = os.path.join(plots_dir, '_'.join(['tripl_mass', save_path_suffix]))
    
    # Different binning
    print('Saving triplet mass distribution')
    utils_topreco.plot_triplet_mass(df_merge, is_ttH, buffer, 0, plot_title, save_path, save_figs)
    utils_topreco.plot_triplet_mass(df_merge, is_ttH, buffer, 1, plot_title, save_path, save_figs)
    
    print("Total time = {} seconds".format(time.time() - t0))
    

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Write GNN top candidates to csv')
    add_arg = parser.add_argument
    
    add_arg('--model', type=str, help='directory with trained model')
    add_arg('--model_plot', type=str, help='abbreviation for model on plot')
    add_arg('--sample', type=str, help='application sample')
    add_arg('--preselection', type=str, help='preselection for efficiency and plots')
    
    add_arg('--thresh', type=float, help='edge score threshold', default = 0.02)
    add_arg('--score_criteria', type=str, help='criteria to score triplets', default = 'sum')
    add_arg('--overlap', type=str, help='whether triplets within an event can have overlapping jets', default = 'True')
    add_arg('--num_triplets', type=int, help='number of triplets are reconstructed per event', default = 2)
    
    add_arg('--save_figs', type=str, help='whether to save plots', default = 'True')
    add_arg('--calculate_efficiency', type=str, help='whether to calculate efficiency', default = 'True')
    
    args = parser.parse_args()
    overlap = (args.overlap == 'True')
    save_figs = (args.save_figs == 'True')
    calculate_efficiency = (args.calculate_efficiency == 'True')

    valid_criteria = ['sum', 'product']
    if args.score_criteria not in valid_criteria:
        raise ValueError('Invalid score criteria, must be one of the following: {}'.format(valid_criteria))
    
    eval_top_candidates(args.model, args.model_plot, args.sample, args.preselection, args.thresh, args.score_criteria, overlap, args.num_triplets, save_figs, calculate_efficiency)
    
 
