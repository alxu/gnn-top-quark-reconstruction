import os
import glob
import pickle
import re
import random


class dataset:
    def __init__(self, input_dir, output_dir, file_size, filter_key = ''):
        
        if filter_key == '':
            f_train = 'train_forder.txt'
            f_test = 'test_forder.txt'
        else:
            f_train = 'train_forder_' + filter_key + '.txt'
            f_test = 'test_forder_' + filter_key + '.txt'
            
        if not os.path.exists(os.path.join(output_dir, f_train)):
        
            self.flist_train = glob.glob(os.path.join(input_dir, '*train*.graphs'))
            self.flist_test = glob.glob(os.path.join(input_dir, '*test*.graphs'))
            
            self.flist_train = [f for f in self.flist_train if filter_key in f]
            self.flist_test = [f for f in self.flist_test if filter_key in f]

            random.shuffle(self.flist_train)
            random.shuffle(self.flist_test)
      
            forder_train = open(os.path.join(output_dir, f_train), "w+")
            forder_test = open(os.path.join(output_dir, f_test), "w+")
        
            for x in self.flist_train:
                forder_train.write(x + '\n')
            forder_train.close()

            for x in self.flist_test:
                forder_test.write(x + '\n')
            forder_test.close()
        
        else:
            forder_train = open(os.path.join(output_dir, f_train), "r")
            forder_test = open(os.path.join(output_dir, f_test), "r")
            
            self.flist_train = forder_train.read().splitlines()
            self.flist_test = forder_test.read().splitlines()
            
        self.f_idx_train = 0
        self.f_idx_test = 0
        self.epoch_train = 0
        self.epoch_test = 0

        self.train_graphs = pickle.load( open(self.flist_train[self.f_idx_train],'rb') )
        self.test_graphs = pickle.load( open(self.flist_test[self.f_idx_test],'rb') )
        
        self.num_events = file_size * len(self.flist_train)
        self.count_events_tr = len(self.train_graphs)
        self.count_events_te = len(self.test_graphs)
        
        self.tr_idx = -1
        self.te_idx = -1
    
    
    def generate_nxgraph(self, is_training=True):
        result = None
        if is_training:
            
            if self.tr_idx <= len(self.train_graphs)-2:
                self.tr_idx += 1
                result = self.train_graphs[self.tr_idx]
            else:

                if self.f_idx_train < len(self.flist_train)-1:
                    self.f_idx_train += 1
                    self.train_graphs = pickle.load( open(self.flist_train[self.f_idx_train],'rb') )
                    if self.epoch_train == 0:
                        self.count_events_tr += len(self.train_graphs)
                else:
                    # start next epoch
                    self.epoch_train += 1
                    self.f_idx_train = 0
                    self.train_graphs = pickle.load( open(self.flist_train[self.f_idx_train],'rb') )

                # start next file
                self.tr_idx = 0
                result = self.train_graphs[self.tr_idx]
        else:   

            if self.te_idx <= len(self.test_graphs)-2:
                self.te_idx += 1
                result = self.test_graphs[self.te_idx]
            else:
                if self.f_idx_test < len(self.flist_test)-1:
                    self.f_idx_test += 1
                    self.test_graphs = pickle.load( open(self.flist_test[self.f_idx_test],'rb') )
                    if self.epoch_test == 0:
                        self.count_events_te += len(self.test_graphs)
                else:
                    self.epoch_test += 1
                    self.f_idx_test = 0
                    self.test_graphs = pickle.load( open(self.flist_test[self.f_idx_test],'rb') )
                self.te_idx = 0
                result = self.test_graphs[self.te_idx]

        return (result[0], result[1]), result[2], result[3] # graphs, event_num, run_num

    
class merged_dataset:
    def __init__(self, dataset_name1, dataset_name2):
        self.dataset1_ = dataset_name1 
        self.dataset2_ = dataset_name2 
        self.num_events = self.dataset1_.num_events + self.dataset2_.num_events
        
        data1_flist_len = len(self.dataset1_.flist_train)
        data2_flist_len = len(self.dataset2_.flist_train)
        self.proportion2_ = data2_flist_len / (data1_flist_len + data2_flist_len) 

    
    def generate_nxgraph(self, is_training=True):
        if is_training and (self.dataset1_.epoch_train > self.dataset2_.epoch_train):  
            return self.dataset2_.generate_nxgraph(is_training)
        elif (not is_training) and (self.dataset1_.epoch_test > self.dataset2_.epoch_test):  
            return self.dataset2_.generate_nxgraph(is_training)
        elif is_training and (self.dataset2_.epoch_train > self.dataset1_.epoch_train):  
            return self.dataset1_.generate_nxgraph(is_training)
        elif (not is_training) and (self.dataset2_.epoch_test > self.dataset1_.epoch_test):  
            return self.dataset1_.generate_nxgraph(is_training)

        rand = random.uniform(0, 1)
        if rand > self.proportion2_:
            return self.dataset1_.generate_nxgraph(is_training)
        else:
            return self.dataset2_.generate_nxgraph(is_training)

