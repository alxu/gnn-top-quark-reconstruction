"""
utils for different preselections
"""
#!/usr/bin/env python
import re
WARNING = 0

def get_num_truth_matched(mychain):
    reco_triplet_1 = mychain.reco_triplet_1
    reco_triplet_2 = mychain.reco_triplet_2
    if (-1 in reco_triplet_1) and (-1 in reco_triplet_2):
        return 0
    elif (-1 not in reco_triplet_1) and (-1 not in reco_triplet_2):
        return 2
    else:
        return 1

# Return true if event passes preselection
def pass_preselection(dataset, mychain, njets, is_training):
    if dataset.use_pre_set1:
        return pass_preselection_set1(dataset, mychain, njets, is_training)
    elif dataset.use_pre_other:
        return pass_preselection_default(dataset, mychain, njets, is_training)
    else:
        print('No preselection option specified')
    

def pass_preselection_set1(dataset, mychain, njets, is_training):
    # Check fold
    evttrain = (mychain.fold % 5 != dataset.idx_.fold4)
    pass_pre = (evttrain == is_training)
    
    num_tm = get_num_truth_matched(mychain)
    pass_case1 = (num_tm == 2) and (mychain.m_nlep == 0) and (mychain.m_nbjet_fixed70 >= 1) and (njets >= 6)
    pass_case2 = (num_tm == 1) and (mychain.m_nlep == 0) and (mychain.m_nbjet_fixed70 >= 1) and (njets >= 3) and (njets < 6)
    pass_case3 = (num_tm == 1) and (mychain.m_nlep == 1) and (mychain.m_nbjet_fixed70 >= 1) and (njets >= 4)
    pass_case4 = (num_tm == 0) and (mychain.m_nlep == 1) and (njets == 3)
    
    pass_pre = pass_pre and (pass_case1 or pass_case2 or pass_case3 or pass_case4)
    return pass_pre


def pass_preselection_default(dataset, mychain, njets, is_training):
    global WARNING
    
    # Check fold
    evttrain = (mychain.fold % 5 != dataset.idx_.fold4)
    pass_pre = (evttrain == is_training)

    # Example: pre_other = 3Nj_Nbj_Nl_0Tm
    pre_other = re.split(r'_', dataset.pre_other)
    bounds = [re.findall(r'(\d*)[A-Za-z]+(\d*)', p)[0] for p in pre_other]
    val = [re.findall(r'\d*([A-Za-z]+)\d*', p)[0] for p in pre_other]

    map_val = {'Nj': 'm_njet', 'Nbj': 'm_nbjet_fixed70', 'Nl': 'm_nlep'}
    for b, v in zip(bounds, val):
        if (v == 'Tm') and dataset.is_ttH_:
            v_chain = get_num_truth_matched(mychain)
        elif v == 'Tm':
            if (WARNING == 0) and ((len(b[0]) > 0) or (len(b[1]) > 0)):
                WARNING = 1
                print('Not a ttH sample: Tm preselection is not applied.')
            continue
        else:
            v_chain = getattr(mychain, map_val[v])

        if len(b[0]) > 0:
            pass_pre = pass_pre and (v_chain >= int(b[0]))
        if len(b[1]) > 0:
            pass_pre = pass_pre and (v_chain < int(b[1]))

    return pass_pre
