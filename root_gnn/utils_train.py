
import numpy as np
import sklearn.metrics
import tensorflow as tf
from graph_nets import utils_tf
from graph_nets import utils_np
import yaml


# Edges only
def create_loss_ops(target_op, output_ops, true_weight = 1, fake_weight = 1):
    weights = target_op.edges * true_weight + (1 - target_op.edges) * fake_weight
    loss_ops = [
        tf.losses.log_loss(target_op.edges, output_op.edges, weights = weights)
        for output_op in output_ops
    ]
    return loss_ops

# Nodes only
def create_loss_node(target_op, output_ops, true_weight = 1, fake_weight = 1):
    equal1 = tf.equal(target_op.nodes, 1)
    equal0 = tf.equal(target_op.nodes, 0)
    equal1 = tf.cast(equal1, tf.int32)
    equal0 = tf.cast(equal0, tf.int32)
    
    weights = tf.multiply(equal1, true_weight) + tf.multiply(equal0, fake_weight)
    loss_ops = [
        tf.losses.log_loss(target_op.nodes, output_op.nodes, weights = weights)
        for output_op in output_ops
    ]
    return loss_ops


# Node and edge
def create_loss_node_edge(target_op, output_ops, true_node_weight = 1, fake_node_weight = 1, true_edge_weight = 1, fake_edge_weight = 1):
    node_weights = target_op.nodes * true_node_weight + (1 - target_op.nodes) * fake_node_weight
    loss_ops = [
        tf.losses.log_loss(target_op.nodes, output_op.nodes, weights = node_weights)
        for output_op in output_ops
    ]
    edge_weights = target_op.edges * true_edge_weight + (1 - target_op.edges) * fake_edge_weight
    loss_ops += [
        tf.losses.log_loss(target_op.edges, output_op.edges, weights = edge_weights)
        for output_op in output_ops
    ]
    return loss_ops


# Edge and global
def create_loss_global_edge(target_op, output_ops, true_global_weight = 1, fake_global_weight = 1, true_edge_weight = 1, fake_edge_weight = 1):
    edge_weights = target_op.edges * true_edge_weight + (1 - target_op.edges) * fake_edge_weight
    loss_ops = [
        tf.losses.log_loss(target_op.edges, output_op.edges, weights = edge_weights)
        for output_op in output_ops
    ]
    global_weights = target_op.globals * true_global_weight + (1 - target_op.globals) * fake_global_weight
    loss_ops += [
        tf.losses.log_loss(target_op.globals, output_op.globals, weights = global_weights)
        for output_op in output_ops
    ]
    return loss_ops


def create_softmax_loss(target_op, output_ops):
    loss_ops = [
        tf.losses.softmax_cross_entropy(target_op.globals, output_op.globals)
        for output_op in output_ops
    ]
    return loss_ops


def make_all_runnable_in_session(*args):
  """Lets an iterable of TF graphs be output from a session as NP graphs."""
  return [utils_tf.make_runnable_in_session(a) for a in args]


def eval_output(target, output, component):
    tdds = utils_np.graphs_tuple_to_data_dicts(target)
    odds = utils_np.graphs_tuple_to_data_dicts(output)

    test_target = []
    test_pred = []
    for td, od in zip(tdds, odds):
        test_target.append(td[component])
        test_pred.append(od[component])

    test_target = np.concatenate(test_target, axis=0)
    test_pred   = np.concatenate(test_pred,   axis=0)
    return test_pred, test_target


def compute_matrics(target, output, component = 'edges'):
    test_pred, test_target = eval_output(target, output, component)
    test_pred = np.array([tp for tp, tt in zip(test_pred, test_target) if tt >= 0])
    test_target = np.array([tt for tt in test_target if tt >= 0])
    
    thresh = 0.5
    y_pred, y_true = (test_pred > thresh), (test_target > thresh)
    return sklearn.metrics.precision_score(y_true, y_pred), sklearn.metrics.recall_score(y_true, y_pred)
