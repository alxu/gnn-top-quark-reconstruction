import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def read_log(file_name, plot_avg, plot_node):
    time_format = '%d %b %Y %H:%M:%S'
    get2nd = lambda x: x.split()[1]

    time_info = []
    data_info = []  
    itime = -1
    if plot_avg:
        data = []
   
    with open(file_name) as f:
        add_time = 0
        for line in f:
            if plot_avg and (line[0:2] == '##'):
                data.append(data_info)
                data_info = []
                itime = -1
            elif line[0] != '#':
                tt = time.strptime(line[:-1], time_format)
                time_info.append(tt)
                data_info.append([])
                itime += 1
                if 'time_consumption' in locals():
                    add_time = time_consumption
            else:
                items = line.split(',')
                try:
                    iteration = int(get2nd(items[0]))
                except ValueError:
                    continue
                
                time_consumption = float(get2nd(items[1])) + add_time
                loss_train = float(get2nd(items[2]))
                loss_test  = float(get2nd(items[3]))
                precision  = float(get2nd(items[4]))
                recall     = float(get2nd(items[5]))
                
                if plot_node:
                    precision_node  = float(get2nd(items[6]))
                    recall_node     = float(get2nd(items[7]))   
                    try:
                        data_info[itime].append([iteration, time_consumption, loss_train, loss_test, precision, recall, precision_node, recall_node])
                    except:
                        print(data_info)
                        print(itime)
                else:
                    try:
                        data_info[itime].append([iteration, time_consumption, loss_train, loss_test, precision, recall])
                    except:
                        print(data_info)
                        print(itime)
    
    if plot_avg:
        return data, time_info
    return data_info, time_info


def plot_log(info, name, increment=1, axs=None):
    fontsize = 16
    minor_size = 14
    if type(info) is not 'numpy.ndarray':
        info = np.array(info)
    df = pd.DataFrame(info, columns=['iteration', 'time', 'loss_train', 'loss_test', 'precision', 'recall'])
    
    # plot one point per increment
    df = df.iloc[::increment]

    # make plots
    if axs is None:
        fig, axs = plt.subplots(2, 2, figsize=(12, 10), constrained_layout=True)
        axs = axs.flatten()

    y_labels = ['Time [s]', 'Training Loss', 'Precision', 'Recall']
    y_data   = ['time', 'loss_train', 'precision', 'recall']
    x_label = 'Iterations'
    x_data = 'iteration'
    for ib, values in enumerate(zip(y_data, y_labels)):
        ax = axs[ib]

        if 'loss_train' == values[0]:
            df.plot(x=x_data, y=values[0], ax=ax, label='Training')
            df.plot(x=x_data, y='loss_test', ax=ax, label='Testing')
            ax.set_ylabel("Losses", fontsize=fontsize)
            ax.legend(fontsize=fontsize)
        else:
            df.plot(x=x_data, y=values[0], ax=ax)
            ax.set_ylabel(values[1], fontsize=fontsize)

        ax.set_xlabel(x_label, fontsize=fontsize)
        ax.tick_params(width=2, grid_alpha=0.5, labelsize=minor_size)
        ax.tick_params(axis='x', rotation=90)

    return axs



def plot_log_node(info, name, increment=1, axs=None):
    fontsize = 16
    minor_size = 14
    if type(info) is not 'numpy.ndarray':
        info = np.array(info)
    df = pd.DataFrame(info, columns=['iteration', 'time', 'loss_train', 'loss_test', 'precision_edge', 'recall_edge', 'precision_node', 'recall_node'])
    
    # plot one point per increment
    df = df.iloc[::increment]

    # make plots
    if axs is None:
        fig, axs = plt.subplots(2, 2, figsize=(12, 10), constrained_layout=True)
        axs = axs.flatten()

    y_labels = ['Time [s]', 'Training Loss', 'Precision', 'Recall']
    y_data   = ['time', 'loss_train', 'precision_edge', 'recall_edge']
    x_label = 'Iterations'
    x_data = 'iteration'
    
    for ib, values in enumerate(zip(y_data, y_labels)):
        ax = axs[ib]

        if 'loss_train' == values[0]:
            df.plot(x=x_data, y=values[0], ax=ax, label='Training')
            df.plot(x=x_data, y='loss_test', ax=ax, label='Testing')
            ax.set_ylabel("Losses", fontsize=fontsize)
            ax.legend(fontsize=fontsize)
        elif 'precision_edge' == values[0]:
            df.plot(x=x_data, y=values[0], ax=ax, label='Edge')
            df.plot(x=x_data, y='precision_node', ax=ax, label='Node')
            ax.set_ylabel(values[1], fontsize=fontsize) # Precision
            ax.legend(fontsize=fontsize)
        elif 'recall_edge' == values[0]:
            df.plot(x=x_data, y=values[0], ax=ax, label='Edge')
            df.plot(x=x_data, y='recall_node', ax=ax, label='Node')
            ax.set_ylabel(values[1], fontsize=fontsize) # Recall
            ax.legend(fontsize=fontsize)
        else:
            df.plot(x=x_data, y=values[0], ax=ax)
            ax.set_ylabel(values[1], fontsize=fontsize)

        ax.set_xlabel(x_label, fontsize=fontsize)
        ax.tick_params(width=2, grid_alpha=0.5, labelsize=minor_size)
        ax.tick_params(axis='x', rotation=90)

    return axs



def plot_log_avg(info, name, axs=None):
    fontsize = 16
    minor_size = 14
    if type(info) is not 'numpy.ndarray':
        info = np.array(info)

    dfs = []
    for i in range(len(info)):
        d = pd.DataFrame(info[i], columns=['iteration', 'time', 'loss_train', 'loss_test', 'precision', 'recall'])
        if i == 0:
            d0 = d.iloc[0]
            dfs.append([i, d0['time'], d0['loss_train'], d0['loss_test'], d0['precision'], d0['recall']])
        dfs.append([i + 1, max(d['time']), np.mean(d['loss_train']), np.mean(d['loss_test']),
                    np.mean(d['precision']), np.mean(d['recall'].iloc[-1])])
        
    df = pd.DataFrame(dfs, columns=['epoch', 'time', 'avg_loss_train', 'avg_loss_test', 'avg_precision', 'avg_recall'])

    # make plots
    if axs is None:
        fig, axs = plt.subplots(2, 2, figsize=(12, 10), constrained_layout=True)
        axs = axs.flatten()

    y_labels = ['Time [s]', 'Average Training Loss', 'Average Precision', 'Average Recall']
    y_data   = ['time', 'avg_loss_train', 'avg_precision', 'avg_recall']
    x_label = 'Epochs'
    x_data = 'epoch'
    for ib, values in enumerate(zip(y_data, y_labels)):
        ax = axs[ib]

        if 'avg_loss_train' == values[0]:
            df.plot(x=x_data, y=values[0], ax=ax, label='Training')
            df.plot(x=x_data, y='avg_loss_test', ax=ax, label='Testing')
            ax.set_ylabel("Average Losses", fontsize=fontsize)
            ax.legend(fontsize=fontsize)
        else:
            df.plot(x=x_data, y=values[0], ax=ax)
            ax.set_ylabel(values[1], fontsize=fontsize)

        ax.set_xlabel(x_label, fontsize=fontsize)
        ax.tick_params(width=2, grid_alpha=0.5, labelsize=minor_size)
        ax.tick_params(axis='x')

    return axs
