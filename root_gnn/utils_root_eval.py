"""
utils for manipulating ROOT samples and application
"""
#!/usr/bin/env python
from __future__ import print_function
import ROOT
import pandas as pd
import numpy as np
from root_numpy import tree2array
import numpy.lib.recfunctions as rfn

import os
import re
import configparser
import glob
import time

import sklearn.metrics
from root_gnn import utils_topreco


# Filter events using preselection
# Example:
# pre = 3Nj_Nbj_0Nl1_2Tm
# 3 <= m_njet, 0 <= m_nlep < 1 (i.e. m_nlep == 0), 2 <= n_truth
def preselection(df, pre, is_ttH):
    
    pre = re.split(r'_', pre)
    bounds = [re.findall(r'(\d*)[A-Za-z]+(\d*)', p)[0] for p in pre]
    val = [re.findall(r'\d*([A-Za-z]+)\d*', p)[0] for p in pre]

    map_val = {'Nj': 'm_njet', 'Nbj': 'm_nbjet_fixed70', 'Nl': 'm_nlep', 'Tm': 'n_truth'}
    for b, v in zip(bounds, val):
        v_col = map_val[v]
        if len(b[0]) > 0:
            df = df[df[v_col] >= int(b[0])]
        if len(b[1]) > 0:
            df = df[df[v_col] < int(b[1])]           
    return df
    

def load_root_dfs(sample, branch_names, pre_root, config_file = ''):
    
    file_dir = os.path.dirname(os.path.realpath(__file__))
    samples_file = os.path.join(file_dir, '../configs/samples.ini')
    samples_config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
    samples_config.read(samples_file)
    
    root_dir = samples_config['General']['root_dir']
    file_sub_dir = samples_config[sample]['sub_dir']
    tree_name = samples_config['General']['tree_name']
    
    if config_file != '':
        config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
        config.read(config_file)
        config_sample = config['Application']['sample']
        if config_sample != sample:
            print('Provided sample and config sample do not match: {}, {}'.format(sample, config_sample))

    file_names = [k[0] for k in samples_config.items(sample)][1:]
    sample_names = [re.sub(r'file_', r'', n) for n in file_names]
    is_ttH = ('ttH' in sample)

    dfs_preselected = []
    for i in range(len(file_names)):
        f = samples_config[sample][file_names[i]]
        f = os.path.join(root_dir, file_sub_dir, f)
    
        df = root_df(f, tree_name, branch_names, pre_root, sample_names[i], is_ttH)
        dfs_preselected += [df]
    
    return dfs_preselected
    
    
    
# Convert root samples into df
def root_df(f, tree_name, branch_names, pre_root, sample, is_ttH):
    
    # Convert TTree to df
    in_file = ROOT.TFile.Open(f, "READ")
    tree = in_file.Get(tree_name)
    array_root = tree2array(tree, branches = branch_names)
    df_root = pd.DataFrame(array_root)

    if is_ttH and ('reco_triplet_1' in branch_names) and ('reco_triplet_2' in branch_names):
        # Expand reco triplets from list to columns
        reco_tripl_1 = np.array(df_root['reco_triplet_1'])
        reco_tripl_2 = np.array(df_root['reco_triplet_2'])

        df_root['bjet1'] = list(map(lambda x: x[0], reco_tripl_1))
        df_root['bjet2'] = list(map(lambda x: x[0], reco_tripl_2))
        df_root['light1_1'] = list(map(lambda x: x[1], reco_tripl_1))
        df_root['light2_1'] = list(map(lambda x: x[1], reco_tripl_2))
        df_root['light1_2'] = list(map(lambda x: x[2], reco_tripl_1))
        df_root['light2_2'] = list(map(lambda x: x[2], reco_tripl_2))

        # Sort jets within triplet
        reco_tripl_1 = list(map(np.sort, reco_tripl_1))
        reco_tripl_2 = list(map(np.sort, reco_tripl_2))

        df_reco1 = pd.DataFrame(reco_tripl_1, columns = ['reco1_1', 'reco1_2', 'reco1_3'])
        df_reco2 = pd.DataFrame(reco_tripl_2, columns = ['reco2_1', 'reco2_2', 'reco2_3'])
        df_root = pd.concat([df_root, df_reco1, df_reco2], axis = 1)

        # Number of valid truth-matched triplets
        df_root['n_truth'] = (df_root['reco1_1'] != -1).astype(int) + (df_root['reco2_1'] != -1).astype(int)

    # Add number of jets
    if 'm_jet_pt' in branch_names:
        df_root['njets'] = df_root['m_jet_pt'].apply(lambda s: len(s))
    
    # Preselection
    df_root = preselection(df_root, pre_root, is_ttH)
    df_root['sample'] = sample
    print('{}: {}'.format(sample, df_root.shape))
    return df_root



# Merge top reconstructed and preselected samples
def merge_topreco_preselected(df_topreco, df_preselected, sample, is_ttH, test_fold, return_efficency_df = False, is_edge_target = True):
    df_merge = pd.merge(df_topreco, df_preselected, 
                        left_on = ['event_num', 'run_num'], 
                        right_on = ['eventNumber', 'runNumber'], 
                        how = 'right')
    df_merge = df_merge.rename(columns = {'fold_y': 'fold'})
    df_merge = df_merge.drop(columns = ['fold_x'])

    na_topreco = df_merge['event_num'].isna()
    if na_topreco.sum() != 0:
        print('Number of events not in topreco, but in preselected root: {}'.format(na_topreco.sum()))

    if is_ttH:
        df_merge_na = df_merge[na_topreco]
        df_merge_na['reco1_match'] = None
        df_merge_na['reco2_match'] = None
        df_merge_na['reco'] = False

        df_merge = df_merge[~na_topreco]

        # Jets sorted in increasing order
        case1 = ((df_merge['jet_1'] == df_merge['reco1_1']) & 
                (df_merge['jet_2'] == df_merge['reco1_2']) & 
                (df_merge['jet_3'] == df_merge['reco1_3']))
        case2 = ((df_merge['jet_1'] == df_merge['reco2_1']) & 
                (df_merge['jet_2'] == df_merge['reco2_2']) & 
                (df_merge['jet_3'] == df_merge['reco2_3']))
            
        if any(case1 & case2):
            print('Duplicate truth-matched triplets in sample {}'.format(sample))

        # True if reconstructed, NA if truth-matched does not exist
        df_merge['reco1_match'] = case1
        df_merge['reco2_match'] = case2
        df_merge['reco'] = (case1 | case2)

        df_merge.loc[df_merge['reco1_1'] == -1, 'reco1_match'] = None
        df_merge.loc[df_merge['reco2_1'] == -1, 'reco2_match'] = None

        df_merge_efficiency = df_merge.copy()
        df_merge = pd.concat([df_merge, df_merge_na])

    df_merge_train_all = df_merge[df_merge['fold'] != test_fold]
    df_merge_test_all = df_merge[df_merge['fold'] == test_fold]
    
    if is_ttH and return_efficency_df:
        return df_merge_train_all, df_merge_test_all, df_merge_efficiency
    return df_merge_train_all, df_merge_test_all, None



def calculate_efficiency_all(dfs_merge_efficiency, dfs_preselected, test_fold, sample):

    # Save values for efficiency calculation
    correct_train_all = np.repeat(0, 3)
    max_train_all = np.repeat(0, 3)
    total_train_all = np.repeat(0, 3)
        
    correct_test_all = np.repeat(0, 3)
    max_test_all = np.repeat(0, 3)
    total_test_all = np.repeat(0, 3)

    correct_train_all_exact = np.repeat(0, 3)
    max_train_all_exact = np.repeat(0, 3)
    total_train_all_exact = np.repeat(0, 3)

    correct_test_all_exact = np.repeat(0, 3)
    max_test_all_exact = np.repeat(0, 3)
    total_test_all_exact = np.repeat(0, 3)


    for df_merge, df_preselected in zip(dfs_merge_efficiency, dfs_preselected):
    
        for tm in range(0, 3):
            correct_train, max_train, total_train, correct_test, max_test, total_test = utils_topreco.calculate_efficiency(
                df_merge, df_preselected, test_fold, sample, tm, exact_tm = False)
            correct_train_exact, max_train_exact, total_train_exact, correct_test_exact, max_test_exact, total_test_exact = utils_topreco.calculate_efficiency(
                df_merge, df_preselected, test_fold, sample, tm, exact_tm = True)

            correct_train_all[tm] += correct_train
            max_train_all[tm] += max_train
            total_train_all[tm] += total_train

            correct_test_all[tm] += correct_test
            max_test_all[tm] += max_test
            total_test_all[tm] += total_test

            correct_train_all_exact[tm] += correct_train_exact
            max_train_all_exact[tm] += max_train_exact
            total_train_all_exact[tm] += total_train_exact

            correct_test_all_exact[tm] += correct_test_exact
            max_test_all_exact[tm] += max_test_exact
            total_test_all_exact[tm] += total_test_exact
    
    
    efficiency_train_all = correct_train_all / total_train_all
    efficiency_test_all = correct_test_all / total_test_all
    max_eff_train_all = max_train_all / total_train_all
    max_eff_test_all = max_test_all / total_test_all

    efficiency_train_all_exact = correct_train_all_exact / total_train_all_exact
    efficiency_test_all_exact = correct_test_all_exact / total_test_all_exact
    max_eff_train_all_exact = max_train_all_exact / total_train_all_exact
    max_eff_test_all_exact = max_test_all_exact / total_test_all_exact

    print('\nEfficiency: Truth-matched & Train-Test')
    for tm in range(0, 3):
        print('tm >= {} train: {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_train_all[tm], total_train_all[tm], 
                      efficiency_train_all[tm], 
                      max_eff_train_all[tm]))
        print('tm >= {} test: {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_test_all[tm], total_test_all[tm], 
                      efficiency_test_all[tm], 
                      max_eff_test_all[tm]))

        print('tm == {} train: {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_train_all_exact[tm], total_train_all_exact[tm], 
                      efficiency_train_all_exact[tm], 
                      max_eff_train_all_exact[tm]))
        print('tm == {} test: {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_test_all_exact[tm], total_test_all_exact[tm], 
                      efficiency_test_all_exact[tm], 
                      max_eff_test_all_exact[tm]))

    print('\nEfficiency: Truth-matched')
    for tm in range(0, 3):
        print('tm >= {} : {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_train_all[tm] + correct_test_all[tm], 
                      total_train_all[tm] + total_test_all[tm], 
                      (correct_train_all[tm] + correct_test_all[tm]) / (total_train_all[tm] + total_test_all[tm]), 
                      (max_train_all[tm] + max_test_all[tm]) / (total_train_all[tm] + total_test_all[tm])
                     ))
        print('tm == {} : {}/{}, {:.4f}, max: {:.4f}'
              .format(tm, 
                      correct_train_all_exact[tm] + correct_test_all_exact[tm], 
                      total_train_all_exact[tm] + total_test_all_exact[tm], 
                      (correct_train_all_exact[tm] + correct_test_all_exact[tm]) / (total_train_all_exact[tm] + total_test_all_exact[tm]), 
                      (max_train_all_exact[tm] + max_test_all_exact[tm]) / (total_train_all_exact[tm] + total_test_all_exact[tm])
                     ))

