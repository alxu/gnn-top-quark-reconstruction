"""
utils for reconstructed top candidates
"""

from __future__ import print_function
import ROOT
import pandas as pd
import numpy as np
import numpy.lib.recfunctions as rfn
import sklearn.metrics

import os
import re
import configparser
import glob

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def calculate_efficiency(df_merge, df_preselected, test_fold, sample, tm, exact_tm = True):
    
    df_merge = df_merge[df_merge['n_truth'] >= tm]
    if exact_tm:
        df_merge = df_merge[df_merge['n_truth'] == tm]
    
    df_merge_train = df_merge[df_merge['fold'] != test_fold]
    df_merge_test = df_merge[df_merge['fold'] == test_fold]

    correct_train = sum(df_merge_train['reco1_match'] == 1) + sum(df_merge_train['reco2_match'] == 1)
    correct_test = sum(df_merge_test['reco1_match'] == 1) + sum(df_merge_test['reco2_match'] == 1)

    train = df_preselected[df_preselected['fold'] != test_fold]
    test = df_preselected[df_preselected['fold'] == test_fold]
    
    if exact_tm:
        total_train = train[train['n_truth'] == tm].shape[0] * 2
        total_test = test[test['n_truth'] == tm].shape[0] * 2
    else:
        total_train = train[train['n_truth'] >= tm].shape[0] * 2
        total_test = test[test['n_truth'] >= tm].shape[0] * 2
    
    # max theoretical efficiency when allow overlapping jets
    if exact_tm:
        if tm == 2:
            max_train = total_train
            max_test = total_test
        elif tm == 1:
            max_train = total_train / 2
            max_test = total_test / 2
        else:
            max_train = 0
            max_test = 0
    else:
        max_train = train[train['n_truth'] == 2].shape[0] * 2
        max_test = test[test['n_truth'] == 2].shape[0] * 2
        if tm < 2:
            max_train += train[train['n_truth'] == 1].shape[0]
            max_test += test[test['n_truth'] == 1].shape[0]
    
    efficiency_train = correct_train / total_train
    efficiency_test = correct_test / total_test
    
    return correct_train, max_train, total_train, correct_test, max_test, total_test



def plot_num_reco_tripl(counts, plot_title, save_path, save_figs = False, show = False):
    props = counts / sum(counts)
    
    plt.figure()
    plt.bar(np.arange(0, 5), props)
    plt.xticks(np.arange(0, 5))
    plt.xlabel('Number of reconstructed triplets')
    plt.ylabel('Proportion of events')
    plt.title('Reconstructed Triplets \n {}'.format(plot_title))

    if save_figs:
        plt.savefig(save_path, bbox_inches = "tight");
    if show:
        plt.show();
       
        
        
def plot_edges_all(edge_preds, is_ttH, increment, buffer, plot_title, save_path, save_figs = False, show = False):
    
    plt.figure()
    bins = np.arange(0, 1 + buffer, increment)

    if is_ttH:    
        pred_true = edge_preds[edge_preds['target'] == True]['pred']
        pred_false = edge_preds[edge_preds['target'] == False]['pred']
        weights_true = np.repeat(1 / len(pred_true), len(pred_true))
        weights_false = np.repeat(1 / len(pred_false), len(pred_false))

        plt.hist(pred_true, bins = bins, weights = weights_true, histtype = 'step', label = 'True edges')
        plt.hist(pred_false, bins = bins, weights = weights_false, histtype = 'step', label = 'False edges')
        plt.legend()
    else:
        weights = np.repeat(1 / len(edge_preds), len(edge_preds))
        plt.hist(edge_preds, bins = bins, weights = weights, histtype = 'step')

    plt.xticks(np.arange(0, 1 + buffer, increment * 5))
    plt.ylabel('Proportion of edges')
    plt.xlabel('Edge scores')
    plt.title('Edge Score Distribution \n {}'.format(plot_title))

    if save_figs:
        plt.savefig(save_path, bbox_inches = "tight");
    if show:
        plt.show();

        
        
def plot_edge_scoring(df_merge, is_ttH, thresh, score_criteria, score_criteria_col, buffer, legend_loc, plot_title, save_path, save_figs = False, show = False):
    
    plt.figure()
    title = score_criteria.title()
    if score_criteria == 'sum':
        if thresh == 0.5:
            increment = 0.05
        else:
            increment = 0.1
        lower = int(3 * thresh / increment) * increment
        upper = 3
    else: # product
        increment = 0.05
        lower = 0
        upper = 1

    bins = np.arange(lower, upper + buffer, increment)

    if is_ttH:
        pred_true = df_merge[df_merge['reco'] == True][score_criteria_col]
        pred_false = df_merge[df_merge['reco'] == False][score_criteria_col]
        weights_true = np.repeat(1 / len(pred_true), len(pred_true))
        weights_false = np.repeat(1 / len(pred_false), len(pred_false))

        plt.hist(pred_true, bins = bins, weights = weights_true, histtype = 'step', label = 'True candidates')
        plt.hist(pred_false, bins = bins, weights = weights_false, histtype = 'step', label = 'False candidates')
        plt.legend(loc = legend_loc)
    else:
        pred = df_merge[score_criteria_col]
        weights = np.repeat(1 / len(pred), len(pred))
        plt.hist(pred, bins = bins, weights = weights, histtype = 'step')

    plt.xticks(np.arange(lower, upper + buffer, increment * 2))
    plt.xlabel('{} of three edge scores'.format(title))
    plt.ylabel('Proportion of triplets')
    plt.title('Edge Score {} Distribution \n {}'.format(title, plot_title))

    if save_figs:
        plt.savefig(save_path, bbox_inches = "tight");
    if show:
        plt.show();

        
            
def plot_roc_scoring(df_merge, is_ttH, score_criteria, score_criteria_col, plot_title, save_path, save_figs = False, show = False): 
    if not is_ttH:
        print('Non-ttH samples do not have truth-matched information')
        return None
    
    plt.figure()
    true = df_merge['reco'].astype(int)
    pred = df_merge[score_criteria_col]

    if score_criteria == 'sum':
        pred = pred / 3
    
    title = score_criteria.title()
    fpr, tpr, _ = sklearn.metrics.roc_curve(true, pred)
    auc = sklearn.metrics.roc_auc_score(true, pred)

    plt.plot(fpr, tpr)
    plt.plot([0, 1], [0, 1], '--')
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate',)
    plt.title('ROC Curve for Edge Score {} \n {} \n AUC = {:.4f} '.format(title, plot_title, auc))

    if save_figs:
        plt.savefig(save_path, bbox_inches = "tight");
    if show:
        plt.show();
        

        
def compute_triplet_mass(df_merge, increment):
    
    triplet_m = []
    for i in range(len(df_merge)):
        evt = df_merge.iloc[i]

        j1 = evt['jet_1']
        j2 = evt['jet_2']
        j3 = evt['jet_3']

        jet1 = ROOT.TLorentzVector()
        jet2 = ROOT.TLorentzVector()
        jet3 = ROOT.TLorentzVector()

        jet1.SetPtEtaPhiE(evt['m_jet_pt'][j1], evt['m_jet_eta'][j1], evt['m_jet_phi'][j1], evt['m_jet_E'][j1])
        jet2.SetPtEtaPhiE(evt['m_jet_pt'][j2], evt['m_jet_eta'][j2], evt['m_jet_phi'][j2], evt['m_jet_E'][j2])
        jet3.SetPtEtaPhiE(evt['m_jet_pt'][j3], evt['m_jet_eta'][j3], evt['m_jet_phi'][j3], evt['m_jet_E'][j3])

        triplet = jet1 + jet2 + jet3
        triplet_m += [triplet.M()]
    
    return triplet_m



def plot_triplet_mass(df_merge, is_ttH, buffer, bounds_idx, plot_title, save_path, save_figs = False, show = False):
    
    plt.figure()
    bounds = [[25, 1000, 25], [30, 500, 10]]
    low = bounds[bounds_idx][0]
    high = bounds[bounds_idx][1]
    increment = bounds[bounds_idx][2]
    
    if is_ttH:
        mass_true = df_merge[df_merge['reco'] == True]['triplet_m']
        mass_false = df_merge[df_merge['reco'] == False]['triplet_m']
        weights_true = np.repeat(1 / len(mass_true), len(mass_true))
        weights_false = np.repeat(1 / len(mass_false), len(mass_false))
        
        mass_true_within_bounds = mass_true[(mass_true >= low) & (mass_true < high)]
        mass_false_within_bounds = mass_false[(mass_false >= low) & (mass_false < high)]

        underflow_true =  mass_true[mass_true < low]
        underflow_false =  mass_false[mass_false < low]
        overflow_true = mass_true[mass_true >= high]
        overflow_false = mass_false[mass_false >= high]

        mass_true_plot = np.append(mass_true_within_bounds.values, np.repeat(low - buffer, len(underflow_true)))
        mass_true_plot = np.append(mass_true_plot, np.repeat(high + buffer, len(overflow_true)))
        mass_false_plot = np.append(mass_false_within_bounds.values, np.repeat(low - buffer, len(underflow_false)))
        mass_false_plot = np.append(mass_false_plot, np.repeat(high + buffer, len(overflow_false)))
        
        # print('True triplets: num {}, min {:.4f}, max {:.4f}\n\tunderflow {:.6f}, overflow {:.6f}'
        #      .format(
        #          len(mass_true), min(mass_true), max(mass_true),
        #          len(underflow_true) / len(mass_true),
        #          len(overflow_true) / len(mass_true))
        #     )
        # print('False triplets: num {}, min {:.4f}, max {:.4f}\n\tunderflow {:.6f}, overflow {:.6f}'
        #      .format(
        #          len(mass_false), min(mass_false), max(mass_false),
        #          len(underflow_false) / len(mass_false),
        #          len(overflow_false) / len(mass_false))
        #     )
    else:
        mass = df_merge['triplet_m']
        weights = np.repeat(1 / len(mass), len(mass))
        
        mass_within_bounds = mass[(mass >= low) & (mass < high)]
        underflow =  mass[mass < low]
        overflow = mass[mass >= high]
        mass_plot = np.append(mass_within_bounds.values, np.repeat(low - buffer, len(underflow)))
        mass_plot = np.append(mass_plot, np.repeat(high + buffer, len(overflow)))
    
        # print('Triplets: num {}, min {:.4f}, max {:.4f}\n\tunderflow {:.6f}, overflow {:.6f}'
        #      .format(
        #          len(mass), min(mass), max(mass),
        #          len(underflow) / len(mass),
        #          len(overflow) / len(mass))
        #     )
    print('Bounds: low {}, high {}, increment {}'.format(low, high, increment))
    
    tick_incr = increment * 4
    low_tick = high
    while low_tick > low:
        low_tick -= tick_incr
    bins = np.arange(max(low - increment - buffer, 0), 
                         high + increment + buffer, 
                         increment)
    if is_ttH:
        plt.hist(mass_true_plot, bins = bins, weights = weights_true, histtype = 'step', label = 'True candidates')
        plt.hist(mass_false_plot, bins = bins, weights = weights_false, histtype = 'step', label = 'False candidates')
        plt.legend()
    else:
        plt.hist(mass_plot, bins = bins, weights = weights, histtype = 'step')

    plt.xticks(np.arange(low_tick, high + buffer, tick_incr))
    plt.xlabel('Mass of reconstructed triplet (GeV)')
    plt.ylabel('Proportion of triplets')
    plt.title('Mass of Reconstructed Triplets \n {}'.format(plot_title))

    if save_figs:
        plt.savefig(save_path + '_low{}_high{}'.format(low, high), bbox_inches = "tight");
    if show:
        plt.show();