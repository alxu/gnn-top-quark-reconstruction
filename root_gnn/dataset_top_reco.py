 
import ROOT
from ROOT import TFile
import os
import numpy as np
import networkx as nx
import random

from . import prepare
from . import utils_dataset
import configparser


class index_mgr:
    def __init__(self, n_total, fold, training_frac=0.8):
        self.fold0 = fold % 5
        self.fold1 = (fold + 1) % 5
        self.fold2 = (fold + 2) % 5
        self.fold3 = (fold + 3) % 5
        # fold for test set
        self.fold4 = (fold + 4) % 5
        
        self.tr_idx = -1
        self.te_idx = -1
        self.total = n_total
        self.all_tr = False
        self.all_te = False
    
    def finished(self, is_training = True):
        if is_training:
            return self.all_tr
        else:
            return self.all_te
    
    def peek(self, is_training=False):
        if is_training:
            return self.tr_idx + 1
        else:
            return self.te_idx + 1
    
    def next(self, is_training=False):
        if is_training:
            self.tr_idx += 1
            if self.tr_idx == self.total:
                self.tr_idx = 0
                self.all_tr = True
            return self.tr_idx
        else:
            self.te_idx += 1
            if self.te_idx == self.total:
                self.te_idx = 0
                self.all_te = True
            return self.te_idx

class dataset:
    def __init__(self, file_name, tree_name, config_file, config_eval_file = '', preselection = '', ttH = True, fold = None, branches = None):
        
        self.file_ = TFile.Open(file_name, 'READ')
        self.tree_ = self.file_.Get(tree_name)
        self.is_ttH_ = ttH
        
        config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
        config.read(config_file)
        
        self.fold_ = config.getint('Training', 'fold', fallback = 0)
        if fold is not None:
            self.fold_ = fold
        
        # Solution
        self.use_node_solution_ = config.getboolean('Solution', 'node', fallback = False)
        self.use_edge_solution_ = config.getboolean('Solution', 'edge', fallback = False)
        self.use_global_solution_ = config.getboolean('Solution', 'global', fallback = False)            
        
        # Attributes
        self.use_btag_ = config.getboolean('Nodes', 'use_pcbtag', fallback = True)
        self.use_edge_mass_ = config.getboolean('Edges', 'use_mass', fallback = True)
        self.use_global_njet_ = config.getboolean('Global', 'use_njet', fallback = True)
        
        # Preselection
        if len(config_eval_file) > 0:
            print('Using config eval for preselection: {}'.format(config_eval_file))
            config = configparser.ConfigParser(interpolation = configparser.ExtendedInterpolation())
            config.read(config_eval_file)
        
        self.use_pre_set1 = config.getboolean('Preselection', 'use_pre_set1', fallback = False)
        self.use_pre_other = config.getboolean('Preselection', 'use_pre_other', fallback = False)
        self.pre_other = config['Preselection']['pre_other']
        
        if len(preselection) > 0:
            print('Using input preselection: {}'.format(preselection))
            self.use_pre_set1 = False
            self.use_pre_other = True
            self.pre_other = preselection

        if branches is not None:
            self.tree_.SetBranchStatus('*', 0)
            for var_name in branches:
                self.tree_.SetBranchStatus(var_name, 1)
            self.tree_.SetBranchStatus('weight', 1)

        # use 80% for training and 20% for testing
        n_total = self.tree_.GetEntries()
        self.idx_ = index_mgr(n_total, self.fold_)
        
    
    def get_num_events(self):
        return self.tree_.GetEntries()
    
    def finished(self, is_training = True):
        return self.idx_.finished(is_training)
    
    # To determine which dataset from merged_dataset to generate next graph
    def generate_rndm(self, is_training = True):
        mychain = self.tree_
        evtid = self.idx_.peek(is_training)
        mychain.GetEntry(evtid)
        return mychain.rndm_order

    
    def generate_nxgraph(self, is_training = True):
        mychain = self.tree_
        
        evtid = self.idx_.next(is_training)
        mychain.GetEntry(evtid)
        m_jet_pt = mychain.m_jet_pt
        njets = mychain.m_njet
        
        while not utils_dataset.pass_preselection(self, mychain, njets, is_training):
            evtid = self.idx_.next(is_training)
            mychain.GetEntry(evtid)
            m_jet_pt = mychain.m_jet_pt
            njets = mychain.m_njet
        
        m_jet_eta = mychain.m_jet_eta
        m_jet_phi = mychain.m_jet_phi
        m_jet_E = mychain.m_jet_E
        m_jet_PCbtag = mychain.m_jet_PCbtag
        
        if self.is_ttH_:
            reco_triplet_1 = mychain.reco_triplet_1
            reco_triplet_2 = mychain.reco_triplet_2
            reco_triplets = [reco_triplet_1, reco_triplet_2]
        
        myEvent = nx.DiGraph()
        node_idx = 0
        scale = [100, 2.5, np.pi, 1]
        if self.use_btag_:
            scale += [1]
        
        for i in range(njets):
            node_feat = [m_jet_pt[i], m_jet_eta[i], m_jet_phi[i], m_jet_E[i]]
            if self.use_btag_:
                node_feat += [m_jet_PCbtag[i]]
                
            if self.is_ttH_ and self.use_node_solution_:
                target = 0
                if (i in reco_triplets[0]) or (i in reco_triplets[1]):
                    target = 1
                myEvent.add_node(node_idx, pos = np.array(node_feat) / np.array(scale), solution = np.array([target]))
            else:
                myEvent.add_node(node_idx, pos = np.array(node_feat) / np.array(scale))
                
            node_idx += 1

        
        for i in range(node_idx):
            for j in range(i + 1, node_idx):
                delta_eta = myEvent.node[j]['pos'][1] - myEvent.node[i]['pos'][1]
                delta_phi = prepare.calc_dphi(myEvent.node[j]['pos'][2], myEvent.node[i]['pos'][2])
                delta_r = np.sqrt(delta_eta ** 2 + delta_phi ** 2)

                edge_feat_ij = [delta_eta,  delta_phi, delta_r]
                edge_feat_ji = [-delta_eta, -delta_phi, delta_r]
                
                if self.use_edge_mass_:
                    jet1 = ROOT.TLorentzVector()
                    jet2 = ROOT.TLorentzVector()
                    jet1.SetPtEtaPhiE(m_jet_pt[i], m_jet_eta[i], m_jet_phi[i], m_jet_E[i])
                    jet2.SetPtEtaPhiE(m_jet_pt[j], m_jet_eta[j], m_jet_phi[j], m_jet_E[j])
                    
                    mass = (jet1 + jet2).M()
                    edge_feat_ij += [mass]
                    edge_feat_ji += [mass]

                target = 0
                if self.is_ttH_:
                    if (i in reco_triplets[0]) and (j in reco_triplets[0]):
                        target = 1
                    elif (i in reco_triplets[1]) and (j in reco_triplets[1]):
                        target = 1

                myEvent.add_edge(i,j, distance = np.array(edge_feat_ij), solution = np.array([target]))
                myEvent.add_edge(j,i, distance = np.array(edge_feat_ji), solution = np.array([target]))
                
        
        if not self.is_ttH_ and self.use_global_solution_:
            raise RuntimeError('Global solution only supported for ttH samples.')
        elif self.use_global_solution_:
            tm_1 = (-1 not in reco_triplet_1)
            tm_2 = (-1 not in reco_triplet_2)
            myEvent.graph['solution'] = np.array([int(tm_1 or tm_2)])
        
        global_feat = njets if self.use_global_njet_ else 0
        myEvent.graph['attributes'] = np.array([global_feat])
            
        return myEvent, mychain.eventNumber, mychain.runNumber

class merged_dataset:
    def __init__(self, datasets):
        self.datasets_ = datasets
        
    def generate_nxgraph(self, is_training=True):
        if is_training:
            all_used = [d.idx_.all_tr for d in self.datasets_]
            if all(all_used):
                for d in self.datasets_:
                    d.idx_.all_tr = False
            all_used = [d.idx_.all_tr for d in self.datasets_]
        else:
            all_used = [d.idx_.all_te for d in self.datasets_]
            if all(all_used):
                for d in self.datasets_:
                    d.idx_.all_te = False
            all_used = [d.idx_.all_te for d in self.datasets_]
        
        num_datasets = len(self.datasets_)
        while True:
            rndm = np.repeat(np.inf, num_datasets)
            for i in range(num_datasets):
                if not all_used[i]:
                    rndm[i] = self.datasets_[i].generate_rndm(is_training)
            
            next_graph = np.argmin(rndm)
            return self.datasets_[next_graph].generate_nxgraph(is_training)
