#!/bin/bash

here=$PWD
model=test
sample=h025_ttH
save_dir=${model//[.]/""}_${sample}
rundir=$here/run/run_eval/$save_dir

echo "Saving in $rundir"
rm -rf $rundir
mkdir -p $rundir

# Copy what you need to the running dir
cp $here/scripts/evaluate.py $rundir
cp $here/configs/config_eval.ini $rundir
cp $here/configs/samples.ini $rundir

for fold in {0..4}; do
    sbatch run_eval_batch.sh $rundir $model $sample $fold
done
