#!/bin/bash 
#SBATCH -q shared
#SBATCH -t 2:00:00
#SBATCH --mem=30GB
#SBATCH -C haswell
#SBATCH -A m3443

rundir=$1
model=$2

cd $rundir
source ~/software/miniconda2/bin/activate env

train_classifier $model
