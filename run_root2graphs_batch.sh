#!/bin/bash 
#SBATCH -q shared
#SBATCH -t 2:00:00
#SBATCH --mem=30GB
#SBATCH -C haswell
#SBATCH -A m3443

runfile=$1
file=$2
name=$3
sample=$4
preselection=$5
config_file=$6

source ~/software/miniconda2/bin/activate env

python $runfile --file $file --name $name --sample $sample --preselection $preselection --is_training 1 --config_file $config_file
python $runfile --file $file --name $name --sample $sample --preselection $preselection --is_training 0 --config_file $config_file
