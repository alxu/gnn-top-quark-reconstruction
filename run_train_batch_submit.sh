#!/bin/bash

model=test

here=$PWD
rundir=$here/run/run_train/$model

echo "Saving in $rundir"
rm -rf $rundir
mkdir -p $rundir

# Copy what you need to the running dir
cp $here/scripts/train_classifier $rundir

sbatch run_train_batch.sh $rundir $model
