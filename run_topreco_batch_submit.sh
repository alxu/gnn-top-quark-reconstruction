#!/bin/bash

root_dir=$PWD
model=test
sample=h025_ttH
is_ttH=True

thresh=0.02
score_criteria=sum
overlap=False
num_triplets=2

thresh_percent=$(echo "$thresh * 100" | bc)
thresh_percent=${thresh_percent%.*}

save_dir=${model//[.]/""}
file_dir=$root_dir/evaluate_csv/$model/$sample
rundir=$root_dir/run/run_topreco/$save_dir/${sample}_thresh${thresh_percent}_${score_criteria}_overlap${overlap}_numtripl${num_triplets}


echo "Saving in $rundir"
rm -rf $rundir
mkdir -p $rundir

# Copy what you need to the running dir
cp $root_dir/scripts/top_candidates.py $rundir

i=0
for f in $file_dir/*fold{0..4}*nodup.csv; do
    sbatch run_topreco_batch.sh $rundir $model $f $i $is_ttH $thresh $score_criteria $overlap $num_triplets
    i=$((i+1))
done
