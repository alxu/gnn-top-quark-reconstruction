#!/bin/bash 
#SBATCH -q shared
#SBATCH -t 5:00:00
#SBATCH --mem=50GB
#SBATCH -C haswell
#SBATCH -A m3443

rundir=$1
model=$2
sample=$3
fold=$4

cd $rundir
source ~/software/miniconda2/bin/activate env

printf '\nEvaluate: model %s sample %s fold%s\n' $model $sample $fold
python -u evaluate.py --fold $fold --config config_eval.ini --samples samples.ini > $fold.txt

